#pragma rtGlobals=1		// Use modern global access method.
#pragma version=6.10 //Version: Oct 30. 2023
#pragma ModuleName = JG_SMPS

	STRCONSTANT SMPSVERSIONDATE = "6.10, 2023.11.09"


	//JG_SMPS_V6.10 General Usage Notes (see version notes below)
	//--Loads and Concatenates SMPS data collected with TSI AIM (built on Mike C's BEARPEX/CalNex JG_SMPS_v3.ipf and used ToolsMJC_v1a.ipf and Tools_20100221.ipf)
	//--As of V6.00 consists of single ipf since functions from DS_GenTools.ipf (general functions) and DD_GenTools.ipf (for cond. sink calcs) are not imported here and renamed with "SMPS_" prefix
	//		--Not template (pxt) distributed now, as it is distributed via GitLab (https://gitlab.com/JimenezGroup/jg-utilities)
	//--(AIM_Version <= 9) Input should be .txt files created as ouput in TSI AIM software (tab-delimited, rows) and dNdLogDp and in named in format: 2010_12_13_1522_SMPS.txt  (yyyy_mm_dd_hhmm_SMPS.txt)
	//--(AIM_Version == 11) Input should be .csv files created as ouput in TSI AIM software (128 channels per decade, comma-delimited, all channels, NOT include raw data) and dNdLogDp.
	//		--Both "Auto Export" and "Manual Export" can be loaded, but user should replace the file format at about line #450 accordingly (search "//for autoexported csv's"
	//		--("Auto Export", default) "SMPS_XXXXXXXXXXXXX_YYYYMMDD.csv", example: SMPS_3082002227012_20230909 where XXXXXXXXXXXXX is the instrument serial number	
	//		--("Manual Export") "YYYY-MM-DD_hhmmss_SMPS.csv", example: 2023-10-31_191240_SMPS.csv
	//		--Currently the code cannot handle mixed file naming, but one could rename the files according to either file format naming, however if there are overlapping/repeated timestamps likely won't work
	//--Starting with .ipf
	//		0) Compile
	//		1)"SMPS Plotting Panel" from "SMPS" menu dropdown
	//		2) If using a mask wave (e.g. sampling valve state), a) load mask and time waves in "Masterlogger folder" (note that  must delete ExpIDSMPS from Project/SMPS/Plotting if updating)
	//			//starting v5.21, make maske wave preaveraging function (1s=>10s) added which requires "GeneralMacros.ipf" (see comments in "ExpID2SMPS" function)
	//--"Load-Concatenate" button prompts user to select folderpath for text files to load and also promps user to
	//			input assign "project" name to files loaded (will put all data loaded/calculated in folder named as such
	//	- Choose the version of TSI AIM used to export text files (diagnostic columns vary)
	//    - User can chose number of Dp bins/decade to regrid dXdLogDp to at the loading step only. This is useful for when file of different Dp ranges are loaded together.
	//			(when plotting later can chose to use these or the native bins).
	//		- Warning, uses a linear interpolation, not binning, so if much fewer bins than in original data is chosen,
	//			not all data points are used in regridding so statistics may be degraded.
	//			Thus best to chose similar Dp number to original data (which is 64 bins/decade (128 bins/decade for AIM11) as recorded in AIM) and use raw data for final workups, instrument comparison, etc.
	//			Also, note that the integrated time series also used the regridded if selected.
	//--Image Time Series Plots plots number, surface a, rea, volume, or condensational sink rate coefficient distributions
	//	Optional inputs for:
	//		Dp smoothing
	//		Max percentile of data assigned to hottest color in image (helps use full color range avoiding outliers blowing up range)
	//		Interpolation across data gaps (of user-specified duration). Otherwise last scan before a datagap gets stretched/interpolated across extended period
	//		Choosing whether to use regridded or raw dXdLogDp (native scaling). Those waves already made during load/concatentate step.
	//---Integrated #, Surface area, Volume Concentration and Cond Sink rate coefficient plots
	//	Optional inputs: Max/Min Dp over which to integrate total concentration
	//--"CS params" buttons pop up a table to view the current inputs to be used in condensational sink calculations (gas diffusion coeff and MW, sticking coeff, temperature, growthfactor)
	//		all CS params are created already using default values. Temperature and GF can be time-dependant so are the length off the SMPS acquisition time. Others are constant.
	//		CS params are created during loading. If user wishes to alter, overwrite the wave in Project/SMPS/Plotting or directly in popped table. Wave needs to be interpolated on to the SMPS timestamp
	//		It is important to note that the gas diffusion coefficient and sticking coefficient are used for calculating CS so need to run again for different inputs of this parameter
	//		Units are: Temp(K), Diff(cm2/s), Sticking Coeff(unitless), GF(unitless), MW (g/mol)
	//		This button kills any previous CS params tables and makes a new one with name of project in TITLE. If user doesn't want it killed later, control-Y and change table NAME
	//--Plotting average size distributions:
	//		Done by apply marque to image or integrated plot, right-click on marque box, and choose "Plot Avg Size Distribution" dropdown
	//		Then can chose to plot distribution for time range selected by marque or point number (dialog pops up). Can do multiple ranges, with logical expressions (e.g. "not" - see pop up help)
	//		Repeating for different periods will append to like-plot (i.e. one for num, vol, CS, surface area). If you want to stop appending, change window NAME. (control Y)
	//		From marqueing from image plot, it finds the wave plotted on the graph. For selecting from the integrated plots, it wants to find the original waves that were not smoothed or
	//interpolated so user must have run the image plot previously selecting -1 for interp gaps and 0 for smoothing (pop-up will remind user if not done).

	//Additional Notes:
	//---Panel inputs stored in root:Temporary (DO NOT DELETE this folder) - can delete all other folders.
	//--There are several auxillary functions at the bottom that may be useful for QC, exporting data. Use "CutBadDat()" function to "manually" enter bad periods that thereafter will be cut when using panel buttons.
	//- AIM saves times as the start time of the scan. Those are retained here, but user may want to adjust integrated time series by half a scan period to be centered before comparing/interpolating to other datasets.
	//- Image plots are from start scan time to next scan start time, so they are naturally "centered" in time.
	//- However, note that the idea of "centering" to the mid time is a bit subjective/approximate since number may be mostly at begin of scan while mass at end, etc...


	////////////////////////////////////////////////////////////////VERSION NOTES (most recent at top) ///////////////////////////////////////////////////////////////////////////////////////
	//Version Notes for V6_10 (231030)
	//Make so can load AIM 11 data format, but AIM 10 is NOT tested.
	//AIM 11 input should be .csv files created by "Auto Export" or "Manual Export", but user should replace the code if loading "Manual Export" file, see General Usage Notes above
	//Changed default AIM Version and Number of bins/decade in the SMPS Plotting panel to 11 and 128.
	//Changed RegridDp default to 1 (so to regrid, which is required for AIM 11 data)

	//Version Notes for V6_00 (230227)
	//Imported/renamed all functions from DD_Gentools.ipf and DS_Gentools.ipf so only consists of a single ipf now (no longer generate pxt since distribution now thru GitLab)
	//Add versioning/date text to panel generation code to be pulled from string constant at top of this ipf
	//Made picture statement (CU logo) static (per PCJ recommendation, so it is compatiable with usingin Squirrel due to Igor version updates)

	//Version Notes for V5_21 (180621-180710)
	//Modified code where Diagnostics are assigned in JG_SMPSParseFunc to ensure always assigned even when first scan number IS NOT 1.
	//this is important when exporting to text a subset of files in AIM
	//Also revised ExpID2SMPS() function (added progress counter and preaveraging). Requires "GeneralMacros.ipf" for valve state preaveraging function (turned of by default)

	//Version Notes for V5_20 (160817-161112):
	//changed the "_final_" to "_ID" for naming image plot variable names since smoothing to >9 made too many characters in wave name string. This should shorten name by 4 digits
	//Fixed some errors that did not allow compilation/running in Igor 7

	//Version Notes for V5_10 (160805):
	//Tools to plot average size distributions added (see usage notes above)
	//A few bugs fixed related to integrated plots plotting wrong parameters (num vs vol vs surface area)

	//Version Notes for V5_00 (160708-160721):
	//DD_GenTools.ipf also needed in addition to DS_GenTools.ipf
	//surface area and condensational sink rate coefficient size distributions and integrated time series added (see Palm et al., ACP, 2015 for info on CS)
	//No longer need to run Populatepanel when starting with ipf since now the Temporary folder with panel/calculation inputs is made and poulated when making panel
	//All filtering/muiltiplexing naming has been changed from ValveState to ExpID (or variations) to be more general.
	//Project automatically slected in panel dropdown after loading new dataset into new project
	//Changed all mass-named variables to be named volume, V, etc.
	//Removed all "scaled" matrices and vectors that were created in NoQA but JG_SMPSMapRawWaves() but never used. User should do any averaging to new timestamp post-processing
	//Changed wave naming in image and integrated plot functions to generic "X" rather than using N for N/V/C/CS to avoid in debugging and retained waves
	//"Limits on Integration Range" set to max/min for project selected (update when loading new data or reselecting project from dropdown)
	//SMPS Plotting Panel only regenerated from menu if it doesn't already exist - otherwise just brought to front.
	//Regridding done as bins/per decade (instead of fixed number of points inputs as before).

	//Version Notes for v4.46; (140714-140806):
	//Changed functions previously in JG_GenTools to have prefixes "DS_" and confirmed that runs with DS_GenTools.ipf Version 5 (July 17, 2014)

	//Version Notes for V4_45 (140618-140707):
	//All functions previously used in DougTools.ipf (largely from MikeTools) confirmed are in or were added to JG_GenTools.ipf.
	//From now on SMPS ipf should be used JG_GenTools.ipf. DougTools no longer needed.
	//A few small changes were needed in the SMPS ipf to deal with indexing errors being outide deminensions of Dm cental and limits waves (se DTS notes)

	//Version Notes for V4_44 (140204-140206):
	//Make so can load up either AIM9 and AIM8+older since saving format is different for AIM9; 4 extra columns logged from SMPS serial
	//Also diagnostics headers are different for AIM5-7, AIM8, AIM9;
	//tested for files exported in v5.5, v7.3, v8.1, v9.0
	//it doesn't matter what CPC was used to log, only the AIM version used to export b/c exported text file will fill in blanks if not recorded
	//added new panel option (during "Load-Concatenate" only) to choose the number of points to regrid Dp to for loading files with different Dp ranges (see Warning in general notes above)
	//added new panel option to use raw or Dp-regridded data for image or integrated concentration plots

	//Version Notes for V4_43 (131025-140122):
	//Fixed problem with negatives at small sizes that sometimes occurred during interpolation to common Dp wave ("JG_SMPSMapRawWaves()") when loading files with different Dp ranges (was interpolating beyond meas range)

	//Version Notes for V4_42 (start 131025):
	//changed numbering of valve state assignment (change to just numbers and user assigns text to list, see "AssignFlags()" at top of ipf

	//Version Notes for V4_4 (start 130615):
	//Rearranged Panel a little, added additional notes
	//Changed valve state assignments to match SOAS multiplexing valve flags

	//Version Notes for V4_3:
	//All functions needed are now in DougTools.ipf

	//Version Notes for V4_1, V4_2:
	// MJC: SMPS functions first written at BEARPEX2007, tidied up for a few bugs at CalNex2010
	// DD 1/12/2011 Edits including:
	//fixed some errors
	//added image plotting functions (number and mass)
	//added integrated plotting functions (number and mass)
	//added panel to control all plotting and concatenation
	// DD July 2011:
	//Capability to import Valve states for multiplexing and split plots into PAM, TD, ambient, etc
	//switch from Mass distributions to Volume (M and "Mass" still used throughout the code though)

	////////////////////////////////////////////////////////////////END VERSION NOTES///////////////////////////////////////////////////////////////////////////////////////

	//Next Versions Update Ideas (feel free to send suggestions to Doug):
	//- moved to Asana task "Update SMPS Code"

menu "SMPS"
	"Load SMPS folder",/q,JG_SMPSImportDir()
end

menu "SMPS"
	"SMPS Plotting Panel",/q, SMPSpanel() //calls populate panel function and then makes panel
end

Menu "GraphMarquee", dynamic  // bbp added this for v5.1

	"-"
	submenu "Plot Avg. Size Distribution"
		"Plot marquee'd range", /q, PlotSizeDist("Marquee")
		"Specify range in marquee'd plot...", /q, PlotSizeDist("Specify")
	end
End

Function PlotSizeDist(typestr)  // bbp added for v5.1
	string typestr

	string destStr,logicstr,todostr,axList,thisAxis, run1Str, run2Str
	variable idex, num, lft, rgt,scanNum,numScans,NaNsRemoved


	// find bottom axis
	axList=axisList("")
	num = itemsinlist(axList)
	do
		thisAxis=stringfromlist(idex,axList)
		getaxis/Q $thisAxis
		if(V_min > 3e9)		// arbitrary, presumes bottom axis is in time
			break
		endif
		idex+=1
	while(idex<num)

	// get marque values
	GetMarquee/Z $thisAxis
	if(v_flag==0)
		abort "No marquee selected! - aborting from PlotSizeDist()"
	endif

	// take info from the graph about which data is involved. identify the time wave
	string WaveStr1 = StringByKey("CWAVEDF",AxisInfo("",thisAxis))
	string WaveStr2 = StringByKey("CWAVE",AxisInfo("",thisAxis))
	string WaveStr3 = StringByKey("CWAVE",AxisInfo("","left"))
	wave TimeWv = $(WaveStr1+WaveStr2)

	// make data folder to hold the Avg Size Dists
	string SizeDistPath = WaveStr1+"SizeDists"
	NewDataFolder/O $(SizeDistPath)

	// convert marquee values to point values
	num=numpnts(TimeWv)-1
	lft = v_left>=TimeWv[0]?(ceil(binarysearchinterp(TimeWv,V_left))):(0)
	rgt=v_right<=TimeWv[num]?(binarysearch(TimeWv,V_right)):(numpnts(TimeWv)-1)
	rgt-=1
	if(rgt<lft)
		abort "No scans selected. Use the marquee to select scans."
	endif


	string ExpIDStr = ""
	string DataStr = ""
	string SmStr = ""
	string IntStr = ""
	ExpIDStr = stringfromlist(1,WaveStr3,"_")

	If(itemsinlist(WaveStr3, "_")>2)
		IntStr = stringfromlist(2,WaveStr3,"_")
		SmStr = stringfromlist(3,WaveStr3,"_")
		DataStr = replacestring("dummyd",stringfromlist(0,WaveStr3,"_"),"")
	else
		IntStr = "intN"
		SmStr = "sm0"
		DataStr = replacestring("TotConc",stringfromlist(0,WaveStr3,"_"),"")
	endif

	string WhichDp = ""
	if(stringmatch(DataStr,"*Raw*"))
		WhichDp = "Raw"
		DataStr = replacestring("Raw",DataStr,"")
	endif


	//	// figure out what the expID is, for use in wave naming later
	//	//if the active window is an integrated time series, this line will be functional
	//	ExpIDStr = ReplaceString("TotTime_", WaveStr2, "")
	//	//if the active window is an image plot, this line will be functional
	//	ExpIDStr = ReplaceString("ScanLims_int", ExpIDStr, "")
	//	ExpIDStr = ReplaceString("_", ExpIDStr, "")

	// identify the type of data

	//	if(stringmatch(WaveStr3,"*N_*"))  // if it's number data
	//		DataStr = "Num"
	//	elseif(stringmatch(WaveStr3,"*dCS_*") || stringmatch(WaveStr3,"*cCS_*")) // if it's CS data
	//		DataStr = "CS"
	//	elseif(stringmatch(WaveStr3,"*ydS_*") || stringmatch(WaveStr3,"*ncS_*")) // if it's Surf area data
	//		DataStr = "SurfArea"
	//	elseif(stringmatch(WaveStr3,"*V_*")) // if it's volume data
	//		DataStr = "Vol"
	//	else // if it's none of the above
	//		abort "Can't recognize the marquee'd data."
	//	endif

	string MatrixStr = ""
	MatrixStr = "d"+DataStr+"dLogDp"+WhichDp+"_"+IntStr+"_"+SmStr+"_ID"+ExpIDStr //DD change from _final_ to just ID
	wave MatrixWv = $(WaveStr1+MatrixStr)

	//	// using the info taken from the graph, figure out which data waves are involved
	//	string MatrixStr = ""
	//	//if the data in the marquee'd window is number dist, one of these lines will be functional
	//	MatrixStr = ReplaceString("dummydN_Raw_", WaveStr3, "dNdLogDp_Raw_int_sm_final_")  // will be active if the window is the image plot
	//	MatrixStr = ReplaceString("dummydN_", MatrixStr, "dNdLogDp_int_sm_final_")  // will be active if the window is the image plot
	//	MatrixStr = ReplaceString("TotconcN_Raw_", MatrixStr, "dNdLogDp_Raw_int_sm_final_")   // will be active if the window is the integrated number
	//	MatrixStr = ReplaceString("TotconcN_", MatrixStr, "dNdLogDp_int_sm_final_")   // will be active if the window is the integrated number
	//	//if the data in the marquee'd window is CS dist, one of these lines will be functional
	//	MatrixStr = ReplaceString("dummydCS_Raw_", MatrixStr, "dCSdLogDp_Raw_int_sm_final_")  // will be active if the window is the image plot
	//	MatrixStr = ReplaceString("dummydCS_", MatrixStr, "dCSdLogDp_int_sm_final_")  // will be active if the window is the image plot
	//	MatrixStr = ReplaceString("TotconcCS_Raw_", MatrixStr, "dCSdLogDp_Raw_int_sm_final_")   // will be active if the window is the integrated number
	//	MatrixStr = ReplaceString("TotconcCS_", MatrixStr, "dCSdLogDp_int_sm_final_")   // will be active if the window is the integrated number
	//	//if the data in the marquee'd window is Surf Area dist, one of these lines will be functional
	//	MatrixStr = ReplaceString("dummydS_Raw_", MatrixStr, "dSdLogDp_Raw_int_sm_final_")  // will be active if the window is the image plot
	//	MatrixStr = ReplaceString("dummydS_", MatrixStr, "dSdLogDp_int_sm_final_")  // will be active if the window is the image plot
	//	MatrixStr = ReplaceString("TotconcS_Raw_", MatrixStr, "dSdLogDp_Raw_int_sm_final_")   // will be active if the window is the integrated number
	//	MatrixStr = ReplaceString("TotconcS_", MatrixStr, "dSdLogDp_int_sm_final_")   // will be active if the window is the integrated number
	//	//if the data in the marquee'd window is CS dist, one of these lines will be functional
	//	MatrixStr = ReplaceString("dummydV_Raw_", MatrixStr, "dVdLogDp_Raw_int_sm_final_")  // will be active if the window is the image plot
	//	MatrixStr = ReplaceString("dummydV_", MatrixStr, "dVdLogDp_int_sm_final_")  // will be active if the window is the image plot
	//	MatrixStr = ReplaceString("TotconcV_Raw_", MatrixStr, "dVdLogDp_Raw_int_sm_final_")   // will be active if the window is the integrated number
	//	MatrixStr = ReplaceString("TotconcV_", MatrixStr, "dVdLogDp_int_sm_final_")   // will be active if the window is the integrated number
	//	wave MatrixWv = $(WaveStr1+MatrixStr)

	if(!waveexists(MatrixWv))
		abort "Please make the image plot containing "+WaveStr1+MatrixStr+" (with no smoothing or interpolation) before plotting size distributions."
	endif


	make/o/n=(DimSize(MatrixWv,1)) AvgDist = 0
	numScans = 0
	NaNsRemoved = 0

	strswitch (typestr)
		case "Marquee":
			for(scanNum = lft;scanNum<= rgt;scanNum+=1)
				MatrixOp/o thisDist = row(MatrixWv,scanNum);ReDimension/N=(DimSize(MatrixWv,1)) thisDist
				wavestats/q thisDist
				if(V_npnts == 0)
					NaNsRemoved += 1
				else
					AvgDist += thisDist
					numScans +=1
				endif
			endfor

			AvgDist /= numScans

			// naming
			if(lft==rgt)
				duplicate/o AvgDist $(SizeDistPath+":Avg"+DataStr+"Dist_"+ExpIDStr+"_"+SmStr+"_"+num2str(lft))/wave=AvgD
			else
				duplicate/o AvgDist $(SizeDistPath+":Avg"+DataStr+"Dist_"+ExpIDStr+"_"+SmStr+"_"+num2str(lft)+"_"+num2str(rgt))/wave=AvgD
			endif

			if(NaNsRemoved > 0)
				print num2str(NaNsRemoved)+" NaNs were removed when making "+nameofwave(AvgD)
			endif

			break
		case "Specify":
			string RunsInput
			variable thisRun,pStart,pEnd,whichRun
			variable suffix = -1
			Prompt RunsInput, "Enter point numbers to average:"
			Doprompt/Help="Input point numbers, ranges, and exclusions as, e.g., \"5;10-20;not 13;not 15-18;30;\"" "", RunsInput
			if(V_Flag==1)
				abort
			endif

			//build a wave of runs to include
			Make/o/n=0 runs
			for(idex = 0;idex < itemsinlist(RunsInput); idex += 1)
				if(stringmatch(stringfromlist(idex,RunsInput),"!not*"))
					if(itemsinlist(stringfromlist(idex,RunsInput),"-") == 2)
						pStart = Str2Num(stringfromlist(0, stringfromlist(idex,RunsInput), "-"))
						pEnd = Str2Num(stringfromlist(1, stringfromlist(idex,RunsInput), "-"))
						For(whichRun=pStart;whichRun <= pEnd;whichRun+=1)
							Redimension/N=(numpnts(runs)+1) runs
							runs[numpnts(runs)-1] = whichRun
						Endfor
					else
						Redimension/N=(numpnts(runs)+1) runs
						runs[numpnts(runs)-1] = Str2Num(stringfromlist(idex,RunsInput)+";")
					endif
				endif
			endfor

			//remove from the wave list the runs preceded by "not "
			for(idex = 0;idex < itemsinlist(RunsInput); idex += 1)
				if(stringmatch(stringfromlist(idex,RunsInput),"not*"))
					if(itemsinlist(stringfromlist(idex,RunsInput),"-") == 2)
						pStart = Str2Num(stringfromlist(0, replacestring("not ",stringfromlist(idex,RunsInput), ""), "-"))
						pEnd = Str2Num(stringfromlist(1, replacestring("not ",stringfromlist(idex,RunsInput), ""), "-"))
						For(whichRun=pStart;whichRun <= pEnd;whichRun+=1)
							deletepoints BinarySearch(runs,whichRun), 1, runs
						Endfor
					else
						deletepoints BinarySearch(runs,Str2Num(replacestring("not ",stringfromlist(idex,RunsInput), ""))), 1, runs
					endif
				endif
			endfor


			//average the distributions in the runs wave
			for(scanNum = 0;scanNum< numpnts(runs);scanNum+=1)
				thisRun = runs[scanNum]
				MatrixOp/o thisDist = row(MatrixWv,thisRun);ReDimension/N=(DimSize(MatrixWv,1)) thisDist
				wavestats/q thisDist
				if(V_npnts == 0)
					NaNsRemoved += 1
				else
					AvgDist += thisDist
					numScans +=1
				endif
			endfor

			AvgDist /= numScans

			// naming
			if(numpnts(runs)==1)
				duplicate/o AvgDist $(SizeDistPath+":Avg"+DataStr+"Dist_"+ExpIDStr+"_"+SmStr+"_"+Num2Str(runs[0]))/wave=AvgD
			else
				if(waveexists($(SizeDistPath+":Avg"+DataStr+"Dist_"+ExpIDStr+"_"+SmStr+"_"+Num2Str(runs[0])+"plus")))
					do
						suffix += 1
					while(waveexists($(SizeDistPath+":Avg"+DataStr+"Dist_"+ExpIDStr+"_"+SmStr+"_"+Num2Str(runs[0])+"plus"+num2str(suffix))))
					duplicate/o AvgDist $(SizeDistPath+":Avg"+DataStr+"Dist_"+ExpIDStr+"_"+SmStr+"_"+Num2Str(runs[0])+"plus"+num2str(suffix))/wave=AvgD
				else
					duplicate/o AvgDist $(SizeDistPath+":Avg"+DataStr+"Dist_"+ExpIDStr+"_"+SmStr+"_"+Num2Str(runs[0])+"plus")/wave=AvgD
				endif
				print "The wave "+nameofwave(AvgD)+" was made using these runs: "+RunsInput
			endif

			if(NaNsRemoved > 0)
				print num2str(NaNsRemoved)+" NaNs were removed when making "+nameofwave(AvgD)
			endif

			break
	endswitch


	// make or append to the corresponding Dist window
	wave Dp_Plot = $(WaveStr1+"Dp_Plot"+WhichDp)
	DoWindow/F $("Avg"+DataStr+"Dist")
	if(V_Flag==0)
		display/n=$("Avg"+DataStr+"Dist") AvgD vs Dp_Plot as "Avg"+DataStr+"Dist"
		ModifyGraph log(bottom)=1,tick=2,mirror=1,standoff=0,grid(bottom)=1
		if(stringmatch(DataStr,"N"))
			Label left, "dN/dlogD\\Bm\\M (cm\\S-3\\M)"
		elseif(stringmatch(DataStr,"CS"))
			Label left, "dCS/dlogD\\Bm\\M (s\\S-1\\M)"
		elseif(stringmatch(DataStr,"S"))
			Label left, "dS/dlogD\\Bm\\M (\\F'Symbol'm\\F'Times New Roman' m\\S2\\Mcm\\S-3\\M)"
		elseif(stringmatch(DataStr,"V"))
			Label left, "dV/dlogD\\Bm\\M (\\F'Symbol'm\\F'Times New Roman' m\\S3\\Mcm\\S-3\\M)"
		endif
		Label bottom, "D\\Bm\\M (m)"
	else
		appendtograph AvgD vs Dp_Plot
	endif

	killwaves/Z AvgDist,thisDist

End

Function JG_SMPSImportDir()	//imports an entire directory of TSI SMPS files

	Variable offset = date2secs(2010,5,15), delta=300 //Relic of the "scaling" originaling in Mike's code. Leave for now b/c required as input to "DS_LoadDir()". But not used later as no scaled waves are made in NoQA fold

	String path = "root:", ExpName = "Default"

	Prompt ExpName, "Project Name:"
	DoPrompt "Assign name of project, campaign, experiment, etc.", ExpName
	path+=ExpName
	path=SMPS_Colon(path)+"SMPS:"

	SMPS_CreateFolder(path+"Raw")
	Make/o/t/n=1 $path+"Raw:FileNames"
	Wave FileNames = $path+"Raw:FileNames"

	Nvar AIM_ExportVersion = root:Temporary:AIM_ExportVersion ////new variables added for AIM 9 export (dd. 140204) //changed to variable (dd 161110)

	//These are the headers saved in AIM for version BEFORE v9.0 (actually at further glance it looks like this changed when nanoparticle correction and diffusion corrected were added in version 7 or 8, but just keep old one for now
	if (AIM_ExportVersion <= 7)
		Make/o/t/n=17  $path+"Raw:DiagHeaders" = {"StartTime[Igor]","StartTime","StartRow","EndRow","SampleFile","ClassifierModel","DMA Model","DMA Inner Radius [cm]","DMA Outer Radius [cm]","DMA Char Length [cm]","CPC Model","Gas Viscosity [kg/(m*s)]","Mean Free Path [m]","Channels/Decade","MultiCharge Corr","Units","Weight"}
	elseif (AIM_ExportVersion ==8)
		Make/o/t/n=20  $path+"Raw:DiagHeaders" = {"StartTime[Igor]","StartTime","StartRow","EndRow","SampleFile","ClassifierModel","DMA Model","DMA Inner Radius [cm]","DMA Outer Radius [cm]","DMA Char Length [cm]","CPC Model","Gas Viscosity [kg/(m*s)]","Mean Free Path [m]","Channels/Decade","MultiCharge Corr","Nanopart Agg Mobil Anal", "Diffus Corr","Gas Dens","Units","Wt"}
	elseif (AIM_ExportVersion == 9)
		Make/o/t/n=22  $path+"Raw:DiagHeaders" = {"StartTime[Igor]","StartTime","StartRow","EndRow","SampleFile","ClassifierModel","DMA Model","DMA Inner Radius [cm]","DMA Outer Radius [cm]","DMA Char Length [cm]","CPC Model","Ref Gas Visc (Pa*s)","Ref Mean Free Path (m)","Ref Gas T (K)","Ref Gas P (kPa)","Chans/Decade","MultiCharge Corr","Nanopart Agg Mobil Anal", "Diffus Corr","Gas Dens","Units","Wt"}
	elseif (AIM_ExportVersion == 11)
		Make/o/t/n=45  $path+"Raw:DiagHeaders" = {"StartTime[Igor]","StartTime","StartRow","EndRow", "AIM Version", "Dataset Name", "Classifier Model", "Classifier S/N", "Classifier Firmware Version", "Calibration status", "Classifier HV Power Supply", "Neutralizer Model", "Neutralizer S/N", "Impactor Model", "Impactor S/N", "DMA Model", "DMA S/N", "Detector Model", "Detector S/N", "Model 3789 D50", "Nano Enhancer", "Detector Calibration status", "Tube Length (cm)", "Tube Diameter (cm)", "Accessory", "Accessory S/N", "Units", "Weight", "Channels/Decade", "Multiple Charge Correction", "Diffusion Loss Correction", "Reference Gas Viscosity (Pa*s)", "Reference Mean Free Path (m)", "Reference Gas Temperature (K)", "Reference Gas Pressure (kPa)", "Sutherland Constant (K)", "Sampling System Particle Loss Correction", "Effective Lengths (m) of components of the sampling system", "Primary Sampling Tube", "PM2.5 Cyclone", "Flow Splitter", "Dryer", "Secondary Sampling Tube", "Flow Rates (L/min) through the components of the sampling system", "Primary Sampling Tube", "PM2.5 Cyclone", "Flow Splitter", "Dryer", "Secondary Sampling Tube" }
	endif

	if (AIM_ExportVersion < 9)
		SMPS_LoadDir(".txt", JG_SMPSParseFunc, path+"Raw:Date_Time;"+path+"Raw:dNdlogDp;"+path+"Raw:Dp;"+path+"Raw:ScanNumber;"+path+"Raw:Diagnostics", "YYYY_MM_DD_hhmm_SMPS.txt", 0, 0, offset, delta, ListOfWavesDims="1;1;1;1;t", FileWave=FileNames) //DD
	elseif (AIM_ExportVersion == 9)
		SMPS_LoadDir(".txt", JG_SMPSParseFunc, path+"Raw:Date_Time;"+path+"Raw:dNdlogDp;"+path+"Raw:Dp;"+path+"Raw:ScanNumber;"+path+"Raw:Diagnostics;"+path+"Raw:SampleTemp_C;"+path+"Raw:SamplePressure_kPa;"+path+"Raw:MeanFreePath_m;"+path+"Raw:GasViscosity_PaSec", "YYYY_MM_DD_hhmm_SMPS.txt", 0, 0, offset, delta, ListOfWavesDims="1;1;1;1;t;1;1;1;1", FileWave=FileNames) //DD
	elseif (AIM_ExportVersion == 11)
		//for autoexported csv's
		SMPS_LoadDir(".csv", JG_SMPSParseFunc, path+"Raw:Date_Time;"+path+"Raw:dNdlogDp;"+path+"Raw:Dp;"+path+"Raw:ScanNumber;"+path+"Raw:Diagnostics;"+path+"Raw:SampleTemp_C;"+path+"Raw:SamplePressure_kPa;"+path+"Raw:MeanFreePath_m;"+path+"Raw:GasViscosity_PaSec;"+path+"Raw:IntegratedNumber_p_cm3", "SMPS_XXXXXXXXXXXXX_YYYYMMDD.csv", 0, 0, offset, delta, ListOfWavesDims="1;1;1;1;t;1;1;1;1;1", FileWave=FileNames) //SY
		//for manual-exported csv's
		//SMPS_LoadDir(".csv", JG_SMPSParseFunc, path+"Raw:Date_Time;"+path+"Raw:dNdlogDp;"+path+"Raw:Dp;"+path+"Raw:ScanNumber;"+path+"Raw:Diagnostics;"+path+"Raw:SampleTemp_C;"+path+"Raw:SamplePressure_kPa;"+path+"Raw:MeanFreePath_m;"+path+"Raw:GasViscosity_PaSec;"+path+"Raw:IntegratedNumber_p_cm3", "YYYY-MM-DD_hhmmss_SMPS.csv", 0, 0, offset, delta, ListOfWavesDims="1;1;1;1;t;1;1;1;1;1", FileWave=FileNames) //SY
	endif

	Variable n,k
	Wave Dp = $path+"Raw:Dp"
	Wave dNdlogDp = $path+"Raw:dNdlogDp"
	Wave Date_Time = $path+"Raw:Date_Time"
	Wave/t Diagnostics = $path+"Raw:Diagnostics"

	if (dimsize(dp,0)>=2)
		for (n=0;n<dimsize(Dp,0);n+=1)
			for (k=dimsize(Dp,1)-1;k>0;k-=1)
				variable DpVal = dp[n][k]
				variable DpType = numtype(DpVal)
				if (DpType==0)
					break
				endif
			endfor
			dNdlogDp[str2num(Diagnostics[n][2]),str2num(Diagnostics[n][3])][k+1,dimsize(dndlogdp,1)-1] = NaN
		endfor
	endif

	//When loading multiple files the Dp and dNdLogDp get a bunch of nans at the end. Remove
	variable idexx
	make/o/n=(dimsize(Dp,0)) MaxDpLength = nan
	for (idexx=0; idexx<dimsize(Dp,0); idexx+=1)
		make/o/n=(dimsize(Dp,1)) thisDp = Dp[idexx][p]
		wavestats/q thisDp
		MaxDpLength[idexx] = V_npnts
	endfor
	wavestats/q MaxDpLength
	ReDimension/n=(dimsize(Dp,0), V_max) Dp
	ReDimension/n=(dimsize(dNdLogDp,0), V_max) dNdLogDp
	killwaves thisDp, MaxDpLength


	JG_SMPSMapRawWaves(dNdlogDp,Dp, Diagnostics, Date_Time)

	//create some boundary Dp and dXdLogDp from raw data for using with option to (not) regrid Dp for plotting later
	setdatafolder $path+"Raw"
	//////But first check if all Dp ranges are the same//////////////////////////////////////
	wave Dp
	make/o/n=(dimsize(Dp,1)) thisDp = Dp[0][p]
	wavestats/q thisDp
	make/o/n=1 file1minDp = V_min
	make/o/n=1 file1maxDp = V_max
	make/o/n=(dimsize(Dp,0)) samemin = nan
	make/o/n=(dimsize(Dp,0)) samemax = nan
	variable idex
	for (idex=0; idex<(dimsize(Dp,0)); idex+=1)
		make/o/n=(dimsize(Dp,1)) thisDp = Dp[idex][p]
		wavestats/q thisDp
		samemin[idex] = V_min==file1minDp[0]
		samemax[idex] = V_max==file1maxDp[0]
	endfor
	wavestats/q samemin
	make/o/n=1 AllsameMinDp = V_avg==1
	wavestats/q samemax
	make/o/n=1 AllsameMaxDp = V_avg==1
	killwaves/z file1minDp file1maxDp samemin samemax thisDp thismaxDp thisminDp, samemins, samemaxs
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//make boundary wave (assume evenly log-spaced)
	//note that DpRaw and DpbRaw will be garbage if different scan ranges loaded and will not be used (regridded only)
	duplicate/o Dp, DpRaw
	redimension/n=(dimsize(DpRaw,1)) DpRaw
	duplicate/o DpRaw, DpbRaw
	SMPS_DiamMids2Lims(DpRaw)  // BBP 16 Feb 2015 put this line in instead of the previous line, to calculate the Dpb properly
	duplicate/o Dx_lim DpbRaw   // this line was added along with the previous line
	killwaves/z root:Dx_lim, root:Dx_mid, Dx_lim, Dx_mid
	// convert dNdLogDp to volume
	Make/o/n=(dimsize(dNdLogDp,0),dimsize(dNdLogDp,1)) dVdLogDp = nan
	dVdLogDp = SMPS_dM(dNdLogDp,DpbRaw,DpRaw,p,q,1)
	// convert dNdLogDp to surface area
	Make/o/n=(dimsize(dNdLogDp,0),dimsize(dNdLogDp,1)) dSdLogDp = nan
	dSdLogDp = dNdLogDp
	matrixtranspose dSdlogDp
	dSdlogDp *= pi*(1e6*DpRaw[p])^2
	matrixtranspose dSdlogDp
	// convert dNdLogDp to CS (won't make here b/c a bunch of additional coding d not used. just done in plotting functions)

	//expand default temperature and grow factor to SMPS time series length
	wave Temperature_1pt = root:Temporary:Temperature
	wave Growthfactor_1pt = root:Temporary:Growthfactor
	make/o/n=(numpnts(Date_Time)) Temperature = Temperature_1pt
	make/o/n=(numpnts(Date_Time)) GrowthFactor = GrowthFactor_1pt

	//always switch the expt dropdown read on the panel to the expt just loaded and overwrite DFNm in Temporary folder
	string testname =gen_dataFolderList_wCurrDF("root:")
	variable Listnum
	ListNum = 1+ WhichListItem(ExpName+":", testname)//ExpName+":")
	PopupMenu dataDFSel1 win=SMPS_plotting,mode=ListNum
	//set datafolder to the experiment name just loaded
	setdatafolder "root:"+ExpName
	string/g root:Temporary:DFNm = "root:"+ExpName + ":"
	string/g root:Temporary:ExptName = ExpName

	//find min/max Dp for this project data and assign to dropdowns in integrated time series panel region.
	wavestats/q Dp
	make/o/n=1 minDp =  V_min*1e9
	make/o/n=1 maxDp =  V_max*1e9
	duplicate/o minDp, root:Temporary:minInt
	duplicate/o maxDp, root:Temporary:maxInt
	killwaves minDp, maxDp

End


Function JG_SMPSParseFunc(refnum, offset, delta, ListOfWaves)
	Variable refnum, offset, delta
	String ListOfWaves

	Wave Date_Time = $StringFromList(0, ListOfWaves)
	Wave dNdlogDp = $StringFromList(1, ListOfWaves)
	Wave dp = $StringFromList(2, ListOfWaves)
	Wave ScanNumber = $StringFromList(3, ListOfWaves)
	Wave/t Diagnostics = $StringFromList(4, ListOfWaves)

	Variable k = numpnts(Date_Time)	// Current length of data waves
	Variable m = dimsize(dp,0)			// Current length of diagnostics waves
	Variable n, HeaderRows=0, DpRows=0, DpCols, Dpval, val, month, day, year, hourv, minv, secv, Datarows=0
	String lineofdata
	variable numDiags = nan

	//new variables added for AIM 9 export (dd. 140204)
	NVar AIM_ExportVersion = root:Temporary:AIM_ExportVersion //changed to variable (dd. 161110)
	if (AIM_ExportVersion <=7)
		numDiags = 13
	elseif (AIM_ExportVersion ==8)
		numDiags = 16
	elseif (AIM_ExportVersion == 9)
		numDiags = 18
		Wave SampleTemp_C = $StringFromList(5, ListOfWaves)
		Wave SamplePressure_kPa = $StringFromList(6, ListOfWaves)
		Wave MeanFreePath_m = $StringFromList(7, ListOfWaves)
		Wave GasViscosity_PaSec = $StringFromList(8, ListOfWaves)
	elseif (AIM_ExportVersion == 11)
		numDiags = 53
		Wave SampleTemp_C = $StringFromList(5, ListOfWaves)
		Wave SamplePressure_kPa = $StringFromList(6, ListOfWaves)
		Wave MeanFreePath_m = $StringFromList(7, ListOfWaves)
		Wave GasViscosity_PaSec = $StringFromList(8, ListOfWaves)
		Wave IntegratedNumber_p_cm3 = $StringFromList(9, ListOfWaves)	// Total Concentration (#/cm³)
	endif

	ReDimension/n=(m+1,numDiags+4) Diagnostics

	Variable scannum
	Variable samptemp, samppress, meanFP, gasvisc, intnum

	if (AIM_ExportVersion <=9)

	for (n=0; n<numDiags;n+=1)	// Currently loading in all the diags listed in the headers above, plus a column in which to put the time zone.
		FReadline /T="\t" refnum, lineofdata
		FReadline refnum, lineofdata
		Diagnostics[m][n+4] = replaceString("\r",lineofdata,"")
	endfor

	FSetPos refnum, 0	//Go back to start... this way if there are more headers in later versions, will still look for dp wave and load correctly
	
	do
		FReadline /T="\r\n" refnum, lineofdata
		HeaderRows+=1
	while (!stringmatch(lineofdata, "\nSample*"))

	FSetPos refnum, 0
	for (n=0; n<HeaderRows-1; n+=1)
		FReadLine /T="\r\n" refnum, lineofdata
	endfor
	
	do
		FReadline /T="\t" refnum, lineofdata
		if (stringmatch(lineofdata, "scan*"))
			break
		endif
		sscanf lineofdata, "%e", Dpval
		if (Dpval>0)
			DpCols = max(DpRows, dimsize(dp,1))
			//ReDimension/n=(m+1,DpCols) Dp
			ReDimension/n=(m+1,DpCols+1) Dp //dd 130615 b/c was cutting off first Dp

			Dp[m][dprows] = dpval/1e9
			dprows+=1
		endif
					
	while (Dpval!=1)
	SMPS_nan2num(0,nan,source=dp)

	FReadline /T="\r\n" refnum, lineofdata
	
	do
		FReadline /T="\t" refnum, lineofdata
		sscanf lineofdata, "\n%e\t", scannum
		if (!scannum)
			break
		endif
		FReadline /T="\t" refnum, lineofdata
		sscanf lineofdata, "%e/%e/%e", month, day, year
		year+=2000
		FReadline /T="\t" refnum, lineofdata
		sscanf lineofdata, "%e:%e:%e", hourv, minv, secv
		
		if (AIM_ExportVersion == 9) //additional variable columns for AIM9 export (dd. 140204)	
		FReadline /T="\t" refnum, lineofdata
		sscanf lineofdata, "\n%e\t", samptemp //"Sample Temp (C)"
		FReadline /T="\t" refnum, lineofdata		
		sscanf lineofdata, "\n%e\t",  samppress //"Sample Pressure (kPa)"
		FReadline /T="\t" refnum, lineofdata
		sscanf lineofdata, "\n%e\t",  meanFP //"Mean Free Path (m)"
		FReadline /T="\t" refnum, lineofdata
		sscanf lineofdata, "\n%e\t",  gasvisc // "Gas Viscosity (Pa*s)"
		endif
		FReadline /T="\t" refnum, lineofdata //this just a throw-away to skip over empty column labeled "Diameter Midpoint" before data starts (dd.130616)
		ReDimension/n=(k+1) Date_Time
		ReDimension/n=(k+1) ScanNumber
		//ReDimension/n=(k+1, DpCols) dndlogdp
		ReDimension/n=(k+1, DpCols+1) dndlogdp //dd 130615 b/c was cutting off 2nd to last dNdlogDp column (line fixed above for Dp also, b/c both were one short, but dropping different columns - was first Dp value)
		Date_Time[k] = date2secs(year,month,day) + (3600*(hourv)) + (60*minv) + secv
		ScanNumber[k] = ScanNum // Bug fix 2009-02-26
		
		if (AIM_ExportVersion >= 9) //additional variable columns for AIM9 export (dd. 140204)	
			ReDimension/n=(k+1) SampleTemp_C
			ReDimension/n=(k+1) SamplePressure_kPa
			ReDimension/n=(k+1) MeanFreePath_m
			ReDimension/n=(k+1) GasViscosity_PaSec			
			SampleTemp_C[k] = samptemp			
			SamplePressure_kPa[k] = samppress 
			MeanFreePath_m[k] = meanFP 					
			GasViscosity_PaSec[k] = gasvisc 
		endif	
		
//		if (ScanNum==1)
//			Diagnostics[m][0] = num2str(date2secs(year,month,day) + (3600*(hourv)) + (60*minv) + secv)
//			Diagnostics[m][1] = num2str(year)+"/"+num2str(month)+"/"+num2str(day)+" - "+num2str(hourv)+":"+num2str(minv)+":"+num2str(secv)
//			Diagnostics[m][2] = num2str(k)		
//		endif
		if (numtype(str2num(Diagnostics[m][2]))==2) //dd 2018-0621, For case that files were exported as part of an AIM file series such as when Dp range changed within file series
			Diagnostics[m][0] = num2str(date2secs(year,month,day) + (3600*(hourv)) + (60*minv) + secv)
			Diagnostics[m][1] = num2str(year)+"/"+num2str(month)+"/"+num2str(day)+" - "+num2str(hourv)+":"+num2str(minv)+":"+num2str(secv)
			Diagnostics[m][2] = num2str(k)		
		endif
		
		for (n=0; n<dprows; n+=1)
			FReadline /T="\t" refnum, lineofdata
			sscanf lineofdata, "%e", val
	
			if (numtype(val)==0)
				dndlogdp[k][n] = val
			endif
		endfor
		if (dprows<dimsize(dndogdp,1))
			dNdlogDp[k][dprows,dimsize(dndlogdp,1)-1] = NaN
		endif
		FReadline /T="\r\n" refnum, lineofdata
		sscanf lineofdata, "%e", val
		k+=1
	while (scannum)

	elseif (AIM_ExportVersion == 11)

	for (n=0; n<numDiags;n+=1)	// Currently loading in all the diags listed in the headers above, plus a column in which to put the time zone.
		FReadline /T="," refnum, lineofdata
		FReadline refnum, lineofdata
		Diagnostics[m][n+4] = replaceString("\r",lineofdata,"")
	endfor

	FSetPos refnum, 0	//Go back to start... this way if there are more headers in later versions, will still look for dp wave and load correctly

	do
		FReadline /T="\r\n" refnum, lineofdata
		HeaderRows+=1
	while (!stringmatch(lineofdata, "\nScan*")) //SY


	FSetPos refnum, 0
	for (n=0; n<HeaderRows-1; n+=1)
		FReadLine /T="\r\n" refnum, lineofdata
	endfor

	do
		FReadline /T="," refnum, lineofdata
		if (stringmatch(lineofdata, "_1.01*")) // SY

			break
		endif
		sscanf lineofdata, "%e", Dpval
		if (Dpval>0)
			DpCols = max(DpRows, dimsize(dp,1))
			ReDimension/n=(m+1,DpCols+1) Dp
			Dp[m][dprows] = dpval/1e9
			dprows+=1
		endif

	while (Dpval!=1)
	SMPS_nan2num(0,nan,source=dp)

	FReadline /T="\r\n" refnum, lineofdata

	do
		FReadline /T="," refnum, lineofdata
		sscanf lineofdata, "\n%e,", scannum
		if (!scannum)
			break
		endif
		FReadline /T="," refnum, lineofdata
		sscanf lineofdata, "%e/%e/%e %e:%e,", day, month, year, hourv, minv // SY
		secv = 0

			FReadline /T="," refnum, lineofdata
			FReadline /T="," refnum, lineofdata
			FReadline /T="," refnum, lineofdata
			FReadline /T="," refnum, lineofdata
			FReadline /T="," refnum, lineofdata
			FReadline /T="," refnum, lineofdata
			FReadline /T="," refnum, lineofdata
			FReadline /T="," refnum, lineofdata
			FReadline /T="," refnum, lineofdata
			FReadline /T="," refnum, lineofdata

			FReadline /T="," refnum, lineofdata
			sscanf lineofdata, "\n%e,", samptemp //"Sample Temp (C)"
			FReadline /T="," refnum, lineofdata
			sscanf lineofdata, "\n%e,",  samppress //"Sample Pressure (kPa)"
			FReadline /T="," refnum, lineofdata
			FReadline /T="," refnum, lineofdata
			sscanf lineofdata, "\n%e,",  meanFP //"Mean Free Path (m)"
			FReadline /T="," refnum, lineofdata
			sscanf lineofdata, "\n%e,",  gasvisc // "Gas Viscosity (Pa*s)"

			FReadline /T="," refnum, lineofdata
			FReadline /T="," refnum, lineofdata
			FReadline /T="," refnum, lineofdata
			FReadline /T="," refnum, lineofdata
			FReadline /T="," refnum, lineofdata
			FReadline /T="," refnum, lineofdata
			FReadline /T="," refnum, lineofdata
			FReadline /T="," refnum, lineofdata
			FReadline /T="," refnum, lineofdata
			FReadline /T="," refnum, lineofdata
			FReadline /T="," refnum, lineofdata
			FReadline /T="," refnum, lineofdata
			FReadline /T="," refnum, lineofdata
			FReadline /T="," refnum, lineofdata
			FReadline /T="," refnum, lineofdata
			FReadline /T="," refnum, lineofdata
			FReadline /T="," refnum, lineofdata
			FReadline /T="," refnum, lineofdata
			FReadline /T="," refnum, lineofdata
			FReadline /T="," refnum, lineofdata
			FReadline /T="," refnum, lineofdata
			FReadline /T="," refnum, lineofdata
			sscanf lineofdata, "\n%e,",  intnum //"Total Concentration (#/cm³)"
			FReadline /T="," refnum, lineofdata
			FReadline /T="," refnum, lineofdata
			FReadline /T="," refnum, lineofdata

		ReDimension/n=(k+1) Date_Time
		ReDimension/n=(k+1) ScanNumber
		ReDimension/n=(k+1, DpCols+1) dndlogdp //dd 130615 b/c was cutting off 2nd to last dNdlogDp column (line fixed above for Dp also, b/c both were one short, but dropping different columns - was first Dp value)

		Date_Time[k] = date2secs(year,month,day) + (3600*(hourv)) + (60*minv) + secv
		ScanNumber[k] = ScanNum // Bug fix 2009-02-26
			ReDimension/n=(k+1) SampleTemp_C
			ReDimension/n=(k+1) SamplePressure_kPa
			ReDimension/n=(k+1) MeanFreePath_m
			ReDimension/n=(k+1) GasViscosity_PaSec
			ReDimension/n=(k+1) IntegratedNumber_p_cm3

			SampleTemp_C[k] = samptemp
			SamplePressure_kPa[k] = samppress
			MeanFreePath_m[k] = meanFP
			GasViscosity_PaSec[k] = gasvisc
			IntegratedNumber_p_cm3[k] = intnum

		if (numtype(str2num(Diagnostics[m][2]))==2) //dd 2018-0621, For case that files were exported as part of an AIM file series such as when Dp range changed within file series
			Diagnostics[m][0] = num2str(date2secs(year,month,day) + (3600*(hourv)) + (60*minv) + secv)
			Diagnostics[m][1] = num2str(year)+"/"+num2str(month)+"/"+num2str(day)+" - "+num2str(hourv)+":"+num2str(minv)+":"+num2str(secv)
			Diagnostics[m][2] = num2str(k)
		endif

		for (n=0; n<dprows; n+=1)
			FReadline /T="," refnum, lineofdata
			sscanf lineofdata, "%e", val

			if (numtype(val)==0)
				dndlogdp[k][n] = val
			endif
		endfor
		if (dprows<dimsize(dndogdp,1))
			dNdlogDp[k][dprows,dimsize(dndlogdp,1)-1] = NaN
		endif
		FReadline /T="\r\n" refnum, lineofdata
		sscanf lineofdata, "%e", val
		k+=1
	while (scannum)

	endif

	Diagnostics[m][3] = num2str(k-1)

End

Function JG_SMPSMapRawWaves(dNdlogDp,Dp,Diagnostics, Date_Time)
	// MJC 9/20/2007 - Maps RAW SMPS to NoQA folder
	Wave dNdlogDp
	Wave Dp		// assuming a 2D dp wave with different dp values for different times
	Wave/t Diagnostics
	Wave Date_Time

	//Variable DpPnts = 50	// number of points in mapped waves...
	NVAR RegridDp_NumBins = root:Temporary:RegridDp_NumBins
	Variable DpPnts = RegridDp_NumBins	// number of points in mapped waves...

	String DestPath = GetWavesDataFolder(dNdlogDp,1)+":NoQA:"
	SMPS_CreateFolder(DestPath)

	// find limits of Dp from all imported files
	WaveStats/Q Dp
	Variable MinDp = V_min
	Variable MaxDp = V_max

	// deal with mapping of diameters waves to a consistent array
	Variable f = ln(V_max/V_min)
	//New Coding for using specified number of bins per decade to regrid Dp and size distribution matrix
	variable TotBins_Int = round(log(V_max/V_min)*DpPnts) //calculate number of bins needed total for
	Make/o/n=(TotBins_Int) $DestPath+"Dp"/wave=dpmapped
	Make/o/n=(TotBins_Int+1) $DestPath+"Dpb"/wave=dpb
	DpMapped = exp(p*f/(TotBins_Int-1)) * V_min

	SMPS_DiamMids2Lims(DpMapped)
	duplicate/o Dx_lim dpb

	Note/K DpMapped
	Note DpMapped "Log-scale midpoints dp wave mapped from V_max and V_min of "+GetWavesDataFolder(Dp,2)
	Note DpMapped "Created at "+time()+" on "+date()

	Note/K Dpb
	Note Dpb "Log-scale boundary dp wave mapped from V_max and V_min of "+GetWavesDataFolder(Dp,2)
	Note Dpb "Created at "+time()+" on "+date()

	// create destination dXdlogdp waves
	Make/o/n=(dimsize(dndlogdp,0),TotBins_Int) $DestPath+"dNdlogDp_Unscaled"/wave=mapped = nan //revised code for gridding to bins per decade
	Make/o/n=(dimsize(dndlogdp,0),TotBins_Int) $DestPath+"dVdlogDp_Unscaled"/wave=Volun = nan //revised code for gridding to bins per decade
	Make/o/n=(dimsize(dndlogdp,0),TotBins_Int) $DestPath+"dSdlogDp_Unscaled"/wave=surfun = nan //revised code for gridding to bins per decade

	// fill in points in principal distribution wave
	Variable n, sp, ep, k, ep_matrix
	for (n=0; n<dimsize(Diagnostics,0);n+=1)	// loop over different dp's
		sp = str2num(Diagnostics[n][2])
		ep = str2num(Diagnostics[n][3])
		Make/o/n=(dimsize(Dp,1))/free TempDp, TempdNdlogDp
		TempDp = Dp[n][p]
		WaveStats/Q TempDp
		ep_matrix = binarysearch(DpMapped,V_max)
		WaveStats/Q TempDp
		ReDimension/n=(dimsize(dp,1) - V_NumNans) TempDp, TempdNdlogDp

		for (k=sp; k<=ep; k+=1)
			TempdNdlogDp = dNdlogDp[k][p]
			//Mapped[k][0,ep_matrix] = interp(DpMapped[q], TempDp, TempdNdlogDp)	// interpolate distribution to new dp mapping

			//linear interpolation function above allows interpolation beyond lower end of scan range when dealing with scans that started at
			//higher Dp than min Dp for entire dataset resulting in negatives, so reassign these Nans.
			//also note that his a linear interpolation so when going from ~110 pts to 50 pts, essentially half the of the information is being lost,
			//thus bin-averaging would be better but may be much slower. Need to look into this:  dd 140117
			make/o/n=(numpnts(DpMapped)) thisMapped = interp(DpMapped, TempDp, TempdNdlogDp)
			matrixtranspose  thisMapped
			thisMapped = DpMapped<V_min ? nan : thisMapped
			Mapped[k][0,ep_matrix] = thisMapped[q]
		endfor
		killwaves/z thisMapped
	endfor

	// convert to Volume
	VolUn = SMPS_dM(Mapped,dpb,dpmapped,p,q,1)

	// convert to surface area
	SurfUn = SMPS_dS(Mapped,dpmapped,p,q)

	// deal with wave notes
	Note/K Mapped
	Note Mapped, "dNdlogDp wave interpolated to log dp scale given by "+GetWavesDataFolder(dpmapped,2)+" from the various different dp scales used by the TSI logging software"
	Note Mapped, "Created "+date()+" - "+time()
	Note Mapped, "Rows correspond to "+GetWavesDataFolder(Date_Time,2)

	Note/K VolUn
	Note VolUn "dVdlogDp wave generated from "+GetWavesDataFolder(Mapped,2)+" using the SMPS_dM function, rho=1"
	Note VolUn, "Created "+date()+" - "+time()
	Note VolUn, "Rows correspond to "+GetWavesDataFolder(Date_Time,2)

	Note/K SurfUn
	Note SurfUn "dSdlogDp wave generated from "+GetWavesDataFolder(Mapped,2)+" using the SMPS_dS function"
	Note SurfUn, "Created "+date()+" - "+time()
	Note SurfUn, "Rows correspond to "+GetWavesDataFolder(Date_Time,2)

	// Make scalar total waves for each matrix
	make/o $GetWavesDataFolder(Mapped,2)+"_Total"/wave = Ntotal_unscaled
	make/o $GetWavesDataFolder(VolUn,2)+"_Total"/wave = Vtotal_unscaled
	make/o $GetWavesDataFolder(surfUn,2)+"_Total"/wave = Stotal_unscaled
	SMPS_dNdlogDp2Total(mapped,ntotal_unscaled,dpmapped)
	SMPS_dNdlogDp2Total(volun,vtotal_unscaled,dpmapped)
	SMPS_dNdlogDp2Total(surfun,stotal_unscaled,dpmapped)

End

//use this function to create folder structure and waves expected as input for plotting/integrating panel function when not using "Load-Concatenate" button that loads TSI-AIM text data
//This function has variable names specific to PSI SMPS tool box data output. Can make generic with wavenames as inputs
//assumes that currently in the folder with the relavant data to input.
Function CreateFoldsWaves_Manual()
	newdatafolder/o :SMPS
	newdatafolder/o :SMPS:Raw
	newdatafolder/o :SMPS:NoQA
	newdatafolder/o :SMPS:Plot

	//create volume matrix (dVdLogDp) from number matrix
	//duplicate/o dNdlogDpMCC, dVdlogDp_Unscaled
	//dVdlogDp_Unscaled = dM(dNdlogDpMCC, diammidptfullboundaries, DiamMidptFull,p,q,1)	// function call that converts to volume
	SMPS_dN2dV(dNdlogDpMCC, DiamMidptFull)

	wave DiamMidptFull, diammidptfullboundaries
	//convert Dp waves from nm to meters for use in SMPS code
	make/o/n=(numpnts(DiamMidptFull)) DiamMidptFull_meters = DiamMidptFull*10^-9
	make/o/n=(numpnts(diammidptfullboundaries)) diammidptfullboundaries_meters = diammidptfullboundaries*10^-9

	duplicate/o dNdlogDpMCC, :SMPS:NoQA:dNdlogDp_Unscaled
	duplicate/o DiamMidptFull_meters, :SMPS:NoQA:Dp //MIDPOINT diameters
	duplicate/o diammidptfullboundaries_meters, :SMPS:NoQA:Dpb //BOUNDARY diameters
	duplicate/o local_time, :SMPS:Raw:Date_Time
	duplicate/o dVdLogDx :SMPS:NoQA:dVdlogDp_Unscaled

	//killwaves dVdLogDx, TotV, diammidptfullboundaries_meters, DiamMidptFull_meters
End


Function plotImage(PlotType)  //DD 1/5/2011 ... plot image plots of number size distributions
	string PlotType //string that can be "Number", "Volume", "SurfArea", "CondSink"

	SVAR ExpFolder = root:temporary:DFNm
	setdatafolder Expfolder

	SMPS_CreateFolder(":SMPS:Plot:")
	SetDataFolder :SMPS:Plot:

	NVAR smpts_Dp = root:temporary:smpts_Dp
	NVAR maxImageZ_ptile = root:temporary:maxImageZ_ptile
	NVAR max_minutes_interp = root:temporary:max_minutes_interp
	NVAR IDstate = root:temporary:IDstate

	string WhichInterp = ""
	if(max_minutes_interp < 0)
		WhichInterp = "N"
	else
		WhichInterp = num2str(max_minutes_interp)
	endif

	svar PathFromTempDir = root:temporary:DFNm
	string/g Exptpath = PathFromTempDir
	string Plotdatafolder = Exptpath + "SMPS:Plot:"

	string NoQAfolderNm = Exptpath + "SMPS:NoQA:"
	string RawfolderNm = Exptpath + "SMPS:Raw:"
	string/g Expt = Exptpath[5,strlen(Exptpath)-2]

	NVAR RegridDp = root:Temporary:RegridDp //if zero, use raw data, if >1 use regridded Dp data

	//need to check if all of the files loaded have the same Dp values otherwise should abort if choosing to use the raw data
	wave AllsameMinDp = $RawfolderNm+"AllsameMinDp"
	wave AllsameMaxDp = $RawfolderNm+"AllsameMaxDp"
	if (RegridDp==0 && (AllsameMinDp[0]==0 || AllsameMaxDp[0]==0))
		abort "!!Quitting plotting function b/c Dp ranges are different for some files loaded. Need to use re-gridded data or if want to plot raw (recorded/native) data, reload files with different Dp ranges in different projects."
	endif

	//	if (RegridDp>0)						/bbp v5.1 moving this down to the other regrid code below, to make it look nice
	//	wave Dp_glob = $NoQAfolderNm+"Dp"
	//	else
	//	wave Dp_glob = $RawfolderNm+"DpRaw"
	//	endif
	//	duplicate/o Dp_glob, Dp_Plot

	string WhichDp = ""  // bbp v5.1 this string gets inserted into wave and window names later on in this function, depending on Regrid
	if (RegridDp>0)
		WhichDp = ""
		wave Dp_glob = $NoQAfolderNm+"Dp"
		duplicate/o Dp_glob, $("Dp_Plot"+WhichDp)/wave=Dp_Plot
		wave Dpb_glob = $NoQAfolderNm+"Dpb"
		duplicate/o Dpb_glob, Dpb_Plot
		make/o/n=(numpnts(Dpb_Plot)) Dpb_plot_nm = Dpb_plot*1e9
	else
		WhichDp = "Raw"
		wave Dp_glob = $RawfolderNm+"DpRaw"
		duplicate/o Dp_glob, $("Dp_Plot"+WhichDp)/wave=Dp_Plot
		wave Dpb_glob = $RawfolderNm+"DpbRaw"
		duplicate/o Dpb_glob, Dpb_Plot
		make/o/n=(numpnts(Dpb_Plot)) DpbRaw_plot_nm = Dpb_plot*1e9
		wave Dpb_plot_nm = DpbRaw_plot_nm
	endif						// bbp v5.1 end of new regrid code

	if (stringmatch(PlotType, "Number"))
		if (RegridDp>0)
			wave dNdlogDp_Plot_glob = $NoQAfolderNm+"dNdlogDp_Unscaled"
		else
			wave dNdlogDp_Plot_glob = $RawfolderNm+"dNdlogDp"
		endif
		duplicate/o dNdlogDp_Plot_glob, dXdlogDp_Plot
	elseif (stringmatch(PlotType, "CondSink"))									//BBP
		if (RegridDp>0)														//
			wave dNdlogDp_Plot_glob = $NoQAfolderNm+"dNdlogDp_Unscaled"		//
		else																	//
			wave dNdlogDp_Plot_glob = $RawfolderNm+"dNdlogDp"					//
		endif																//
		duplicate/o dNdlogDp_Plot_glob, dXdlogDp_Plot							//
		matrixtranspose dXdlogDp_Plot

		wave Diff = root:temporary:Diff
		wave GF = $RawfolderNm+"Growthfactor"
		wave MW = root:temporary:MW
		wave Alpha = root:temporary:Alpha
		wave Temperature = $RawfolderNm+"Temperature"

		variable vGF, vTemp, vDiff, vMW, vAlpha
		variable idexx
		make/o/n=(numpnts(Dp_plot)) FS_corr
		for (idexx=0; idexx<dimsize(dXdlogDp_plot,1); idexx+=1)
			vGF = GF[idexx]
			vTemp = Temperature[idexx]
			vAlpha = Alpha[idexx]
			vDiff = Diff[idexx]
			vMW = MW[idexx]
			FS_corr = SMPS_FS_corr_calc(vMW,vTemp,vDiff,vAlpha,vGF, Dp_Plot)

			dXdlogDp_Plot[][idexx] *= FS_corr[p]*4*pi*vDiff*vGF*Dp_Plot[p]*100/2 // Diff (cm2/s); GF(unitless); Dp_Plot(m) => convert to cm here; /2 converts diams to radius
		endfor
		matrixtranspose dXdLogDp_plot
		killwaves/z FS_corr, root:Kn

	elseif (stringmatch(PlotType, "SurfArea"))									//BBP
		if (RegridDp>0)														//
			wave dSdlogDp_Plot_glob = $NoQAfolderNm+"dSdlogDp_Unscaled"		//
		else																	//
			wave dSdlogDp_Plot_glob = $RawfolderNm+"dSdlogDp"					//
		endif																//
		duplicate/o dSdlogDp_Plot_glob, dXdlogDp_Plot							//
	elseif (stringmatch(PlotType, "Volume"))
		if (RegridDp>0)
			wave dVdlogDp_Plot_glob = $NoQAfolderNm+"dVdlogDp_Unscaled"
		else
			wave dVdlogDp_Plot_glob = $RawfolderNm+"dVdlogDp"
		endif
		duplicate/o dVdlogDp_Plot_glob, dXdlogDp_Plot
	endif

	//	if (RegridDp>0)							//bbp v5.1 commented this section out, replaced with code above by the other Regrid if statement.
	//	wave Dpb_glob = $NoQAfolderNm+"Dpb"	// bbp the new code allows raw dp and regridded waves to exist separately, so both can be plotted without overwriting the dp wave in image plots (dpb_plot_nm)
	//	else
	//	wave Dpb_glob = $RawfolderNm+"DpbRaw"
	//	endif
	//	duplicate/o Dpb_glob, Dpb_Plot

	wave Date_Time_glob = $RawfolderNm+"Date_Time"
	duplicate/o Date_Time_glob, Date_Time_Plot

	//	wave Date_Time_Plot, Dpb_plot							// bbp v5.1 commented these two lines out. they're not needed after the code changes from a few lines up
	//	make/o/n=(numpnts(Dpb_Plot)) Dpb_plot_nm = Dpb_plot*1e9

	//NaN bad data periods
	CutBadDat()
	//duplicate/o root:QCinfo:BadStart_SMPS, BadStart_SMPS
	//duplicate/o root:QCinfo:BadStart_SMPS, BadEnd_SMPS
	wave BadStart_SMPS, BadEnd_SMPS

	//	variable idexx
	for (idexx=0; idexx<numpnts(BadStart_SMPS); idexx+=1)
		dXdlogDp_Plot = (Date_Time_Plot[p]>=BadStart_SMPS[idexx] && Date_Time_Plot[p]<=BadEnd_SMPS[idexx]) ? NaN : dXdlogDp_Plot[p][q]
	endfor

	///////////////////////////////////--Expt ID State Filtering--////////////////////////////////////////////////
	//pair down time wave and concentration matrix to specific choosen valve state
	if (waveexists(ExpIDSMPS))
		//do nothing
	else
		if (exists("root:Masterlogger:ExpID"))
			ExpID2SMPS() //first run function to pick out experiment ID states for SMPS scan times.
		else
			make/o/n=(numpnts(Date_Time_Plot)) ExpIDSMPS = 99
		endif
	endif

	//for now, assume that if max interp points is -1  then will not remove nan'ed rows for purpose of exporting data on same time stamp and same length as integrated volume or number concentrations
	if (max_minutes_interp < 0)
		string/g CutInterpNaNs = "No"
	else
		string/g CutInterpNaNs = "Yes"
	endif

	if (stringmatch(CutInterpNaNs, "Yes"))
		print "Warning b/c CutInterpNaNs==Yes for plotting purposes dNdLogDp and Integrated Num and Vol arrays are different length"
		//add option to cut out values rather then Nan out for plotting
		make/o/n=(numpnts(Date_Time_Plot)) tempNaN = dXdlogDp_Plot[p][0]
		Matrixop/o tempNaN = sumrows(numtype(dXdlogDp_Plot))
		make /o/d/n=0 temp_dXdlogDp_Plot = NaN
		variable iNan
		for (iNan=0; iNan<numpnts(Date_Time_Plot); iNan+=1)
			if (tempNaN[iNan] != 2*dimsize(dXdLogDp_Plot, 1))
				make/o/n=(dimsize(dXdLogDp_Plot,1),1) this_dXdlogDp_Plot = dXdlogDp_Plot[iNan][p]
				concatenate/NP {this_dXdlogDp_Plot}, temp_dXdlogDp_Plot
			endif
		endfor
		//rewrite paired down waves to original names
		matrixtranspose temp_dXdlogDp_Plot
		duplicate/o temp_dXdlogDp_Plot, dXdlogDp_Plot
		//now pair down time wave
		duplicate/o Date_Time_Plot, tempDate_Time_Plot
		extract Date_Time_Plot, temp_Date_Time_Plot, (tempNaN != 2*dimsize(dXdLogDp_Plot, 1))
		duplicate/o temp_Date_Time_Plot, Date_Time_Plot
		//now pair down ExpID state wave
		duplicate/o ExpIDSMPS, temp_ExpIDSMPS
		extract ExpIDSMPS, temp_ExpIDSMPS, (tempNaN != 2*dimsize(dNdLogDp_Plot, 1))
		duplicate/o temp_ExpIDSMPS, ExpIDSMPS
		killwaves/z temp_dXdlogDp_Plot, this_dXdlogDp_Plot, tempNaN, temp_ExpIDSMPS, tempNaN
	endif


	///////////////////////////////////--ExpID State Filtering--////////////////////////////////////////////////
	wave ExpIDSMPS
	if (IDState==99)
		print "Plotting All Data (no ExpID, etc flags)"
	else
		extract Date_Time_Plot, temp_Date_Time_Plot, (ExpIDSMPS==IDState)
	endif

	if (IDState==99)
		//do nothing - i.e. keep all data
	else
		make /o/d/n=0 temp_dNdlogDp_Plot = NaN
		variable iML
		for (iML=0; iML<numpnts(Date_Time_Plot); iML+=1)
			if (ExpIDSMPS(iML)==IDState)
				make/o/n=(dimsize(dXdLogDp_Plot,1),1) this_dXdlogDp_Plot = dXdlogDp_Plot[iML][p]
				concatenate/NP {this_dXdlogDp_Plot}, temp_dXdlogDp_Plot
			endif
		endfor

		//rewrite paired-down waves to original names
		matrixtranspose temp_dXdlogDp_Plot
		duplicate/o temp_dXdlogDp_Plot, dXdlogDp_Plot
		duplicate/o temp_Date_Time_Plot, Date_Time_Plot
		if  (waveexists(ExpIDSMPS_orig))
			duplicate/o ExpIDSMPS_orig, ExpIDSMPS
		endif

		killwaves/z temp_dXdlogDp_Plot, this_dXdlogDp_Plot, temp_Date_Time_Plot
	endif
	//////////////////////////////--End ExpID State Filtering--////////////////////////////////////////////////

	//copy ID-filtered waves
	if (IDState == 99)
		string/g IDstate_suffix = "all"
	else
		string/g IDstate_suffix = num2str(IDState)
	endif
	if (stringmatch(PlotType, "Number"))
		make/o/t/n=1 NonProcName = "dNdLogDp_" + IDstate_suffix
	elseif (stringmatch(PlotType, "SurfArea"))							//BBP
		make/o/t/n=1 NonProcName = "dSdLogDp_" + IDstate_suffix		//BBP
	elseif (stringmatch(PlotType, "Condsink"))							//BBP
		make/o/t/n=1 NonProcName = "dCSdLogDp_" + IDstate_suffix		//BBP
	elseif (stringmatch(PlotType, "Volume"))
		make/o/t/n=1 NonProcName = "dVdLogDp_" + IDstate_suffix
	endif
	make/o/n=(dimsize(dXdLogDp_Plot,0),dimsize(dXdLogDp_Plot,1)) $NonProcName[0] = dXdLogDp_Plot

	//create start/end wave for bracketing dNdLogDp points for image plots (Date_Time is start times)
	//so assume next start time is end time (and assume that last scan is same duration as second to last scan)
	make/o/d/n=(numpnts(Date_Time_Plot)+1) ScanLims_Plot = Date_Time_Plot[p]
	ScanLims_Plot[numpnts(Date_Time_Plot)] = (Date_Time_Plot[numpnts(Date_Time_Plot)-1]-Date_Time_Plot[numpnts(Date_Time_Plot)-2])/2+Date_Time_Plot[numpnts(Date_Time_Plot)-1]
	SetScale d 0,0,"dat", ScanLims_Plot //So plots as time/date format, not integer

	//deal with gaps in plotting (i.e. need to create ScanLims to span either just scan period or a user-defined
	//maximum period which will in effect be a max interpolation (otherwise greyed for missing data)
	//(use "max_minutes_interp" from panel, //otherwise, if "max_minutes_interp" set to zero use some typicall scan period (median of last XXX scans)
	//duplicate/o dNdLogDp_Plot, dNdLogDp_int
	wave dXdLogDp_Plot
	variable idex
	make /o/d/n=(dimsize(dXdLogDp_Plot,1),1) dummyNaNs = NaN
	duplicate/o dummyNaNs, dXdLogDp_int
	//Redimension/N=0 dNdLogDp_int
	make/o/d/n=1 ScanLims_int = ScanLims_Plot[0]
	//assume that 2nd scan start normal interval after first
	variable lastgoodDur =  (ScanLims_Plot[1]-ScanLims_Plot[0])/60
	variable latestgoodStart = ScanLims_Plot[0]

	for (idex=0;idex<numpnts(ScanLims_Plot)-1;idex+=1)
		//find difference between scans
		variable thisDiff = (ScanLims_Plot[idex+1]-ScanLims_Plot[idex])/60
		//extract this scan size distribution
		make /o/d/n=(dimsize(dXdLogDp_Plot,1),1) junk = dXdLogDp_Plot[idex][p]
		//check if gap too large
		if (thisDiff>(max_minutes_interp+0.1 + lastgoodDur*1.2) && max_minutes_interp>=0)//if gap too large, add extra row to dNdLogDp of NaNs and reasonable end time
			if (idex>0)//just in case large gap after first run
				//make/o/d/n=1 thisEnd =  (ScanLims_Plot[idex]+(ScanLims_Plot[idex]-ScanLims_Plot[idex-1])
				make/o/d/n=1 thisEnd =  (ScanLims_Plot[idex]+lastgoodDur*60)
				if (thisEnd[0]>=ScanLims_Plot[idex+1]) //to ensure that this dummy time isn't beyond next timestamp (i.e. time must be monotonically increasing)
					make/o/d/n=1 thisEnd =  ScanLims_Plot[idex+1]-1
				endif
			else //if first run then just assign average difference
				print "error - need to code in median scan difference here (make median function in tools)"
				make/o/d/n=1 thisEnd = ScanLims_Plot[idex]+(ScanLims_Plot[idex]-ScanLims_Plot[idex-1])
				//*****//make/o/d/n=(numpnts(Date_Time_Plot)-1) thisEnd =  Date_Time_Plot[idex]+median(Date_Time_Plot[p] - Date_Time_Plot[p+1])
			endif
			//			print "adding extra NaNs row to dNdLogDp b/c data gap larger than userdefined period"
			concatenate/NP	{thisEnd}, ScanLims_int
			concatenate/NP	{junk}, dXdLogDp_int
			variable latestgoodEnd = thisEnd[0]
			//determine difference of last normal scan duration for use in next loop
			lastgoodDur = (latestgoodEnd - latestgoodStart)/60
			//add dummy data from this point across gap
			make/o/d/n=1 thisEnd =  ScanLims_Plot[idex+1]
			concatenate/NP	{dummyNaNs}, dXdLogDp_int
			concatenate/NP	{thisEnd}, ScanLims_int
			latestgoodStart = thisEnd[0]
		else  //otherwise, in most cases, just rebuild normally
			make/o/d/n=1 thisEnd =  ScanLims_Plot[idex+1]
			concatenate/NP	{junk}, dXdLogDp_int
			concatenate/NP	{thisEnd}, ScanLims_int
			latestgoodEnd = thisEnd[0]
			lastgoodDur = (latestgoodEnd - latestgoodStart)/60
			latestgoodStart = latestgoodEnd
		endif
	endfor
	matrixtranspose dXdLogDp_int
	deletepoints 0, 1, dXdLogDp_int
	SetScale d 0,0,"dat", ScanLims_int //So plots as time/date format, not integer
	killwaves/z thisDiff, thisEnd, dummyNaNs

	//create smoothed dNdLogDp for image plotting only  (else if zero, no smoothing)
	//duplicate/o dNdLogDp_int, dNdLogDp_int_sm
	//wave dNdLogDp
	variable i
	Duplicate/o dXdLogDp_int, dXdLogDp_int_sm
	for (i=0;i<dimsize(dXdLogDp_int,0);i+=1)
		make /o/d/n=(1,dimsize(dXdLogDp_int,1)) junk = dXdLogDp_int[i][q]
		Smooth/B/E=3 smpts_Dp, junk
		dXdLogDp_int_sm[i][] = junk[i][q]
	endfor
	killwaves/z junk

	//determine percentile ranges to be used for plotting color limits
	Duplicate/o dXdLogDp_int_sm, sortdXdLogDp	// Make a clone of wave
	wavestats/q sortdXdLogDp
	Redimension/N=(V_npnts)  sortdXdLogDp
	Sort sortdXdLogDp, sortdXdLogDp
	wavestats/q sortdXdLogDp
	variable maxImageZ_index = round((V_npnts-1)*maxImageZ_ptile/100)
	variable maxImageZ = sortdXdLogDp(maxImageZ_index)

	//rename variable with ExpID state so when different states plotted they don't update other graphs
	make/o/t/n=1 ScanLims_int_Name = "ScanLims_int"+ WhichInterp + "_" + IDstate_suffix
	//	make/o/t/n=1 ScanLims_Plot_Name = "ScanLims_Plot_" + IDstate_suffix  // bbp v5.1 we think this line and the one two below are relics that can be deleted
	duplicate/o ScanLims_int, $ScanLims_int_Name[0]
	//	duplicate/o ScanLims_plot, $ScanLims_Plot_Name

	if (cmpstr(PlotType, "Number") == 0)
		make/o/t/n=1 ConcNamez = "dNdLogDp"+WhichDp+"_int"+ WhichInterp + "_sm" + num2str(smpts_Dp) + "_ID" + IDstate_suffix	// bbp v5.1 inserted the WhichDp code in 12 places below starting here //DD change from _final_ to just ID
		duplicate/o dXdLogDp_int_sm, $ConcNamez[0]
	elseif (cmpstr(PlotType, "CondSink") == 0)										//BBP Surf Area
		make/o/t/n=1 ConcNamez= "dCSdLogDp"+WhichDp+"_int"+ WhichInterp + "_sm" + num2str(smpts_Dp) + "_ID" + IDstate_suffix		//BBP //DD change from _final_ to just ID
		duplicate/o dXdLogDp_int_sm, $ConcNamez[0]									//BBP
	elseif (cmpstr(PlotType, "SurfArea") == 0)										//BBP Surf Area
		make/o/t/n=1 ConcNamez= "dSdLogDp"+WhichDp+"_int"+ WhichInterp + "_sm" + num2str(smpts_Dp) + "_ID" + IDstate_suffix			//BBP //DD change from _final_ to just ID
		duplicate/o dXdLogDp_int_sm, $ConcNamez[0]									//BBP
	elseif (cmpstr(PlotType, "Volume") == 0)
		make/o/t/n=1 ConcNamez= "dVdLogDp"+WhichDp+"_int"+ WhichInterp + "_sm" + num2str(smpts_Dp) + "_ID" + IDstate_suffix	 		//DD change from _final_ to just ID
		duplicate/o dXdLogDp_int_sm, $ConcNamez[0]
	endif

	//make Number concentration image plot
	if (stringmatch(PlotType, "Number"))
		if (max_minutes_interp>=0)
			make/o/t/n=1 plotnameN = "dN"+WhichDp+"_sm" + num2str(smpts_Dp) + "_int"+ WhichInterp + "_" + Expt + IDstate_suffix
		else
			make/o/t/n=1 plotnameN = "dN"+WhichDp+"_sm" + num2str(smpts_Dp) + "_int"+WhichInterp+"_" + Expt + IDstate_suffix
		endif
		make/o/n=(numpnts($ScanLims_int_Name[0])) $("dummydN"+WhichDp + "_" + IDstate_suffix+"_int"+WhichInterp+"_sm"+num2str(smpts_Dp))/wave=dummy = 1  // bbp v5.1
		Display/n=$plotnameN[0] dummy vs $ScanLims_int_Name[0];ModifyGraph hideTrace=1;AppendImage $ConcNamez[0] vs {$ScanLims_int_Name[0],Dpb_plot_nm};DelayUpdate	// bbp v5.1
		///set "smart" concentration scale limits based on XXX percentile range of data plotted ("maxImageZ_ptile" from panel)
		ModifyImage  $ConcNamez[0] ctab= {0,maxImageZ,Rainbow,1}
		ColorScale/C/N=text0/A=RC image=$ConcNamez[0]
		ColorScale/C/N=text0 "dNdlogDm (cm\\S-3\\M)"
	elseif (stringmatch(PlotType, "CondSink"))																					//BBP Cond Sink
		if (max_minutes_interp>=0)																							//BBP
			make/o/t/n=1 plotnameCS = "dCS"+WhichDp+"_sm" + num2str(smpts_Dp) + "_int"+ WhichInterp + "_" + Expt + IDstate_suffix	//BBP
		else																													//BBP
			make/o/t/n=1 plotnameCS = "dCS"+WhichDp+"_sm" + num2str(smpts_Dp) + "_int"+WhichInterp+"_" + Expt + IDstate_suffix									//BBP
		endif																												//BBP
		make/o/n=(numpnts($ScanLims_int_Name[0])) $("dummydCS"+WhichDp + "_" + IDstate_suffix+"_int"+WhichInterp+"_sm"+num2str(smpts_Dp))/wave=dummy = 1  // bbp v5.1
		Display/n=$plotnameCS[0] dummy vs $ScanLims_int_Name[0];ModifyGraph hideTrace=1;AppendImage $ConcNamez[0] vs {$ScanLims_int_Name[0],Dpb_plot_nm};DelayUpdate	// bbp v5.1
		///set "smart" concentration scale limits based on XXX percentile range of data plotted ("maxImageZ_ptile" from panel)			//BBP
		ModifyImage $ConcNamez[0] ctab= {0,maxImageZ,Rainbow,1}																//BBP
		ColorScale/C/N=text0/A=RC image = $ConcNamez[0]																		//BBP
		ColorScale/C/N=text0 "dCSdlogDm (s\S-1\M)"																			//BBP
	elseif (stringmatch(PlotType, "SurfArea"))																					//BBP Surf Area
		if (max_minutes_interp>=0)																							//BBP
			make/o/t/n=1 plotnameS = "dS"+WhichDp+"_sm" + num2str(smpts_Dp) + "_int"+ WhichInterp + "_" + Expt + IDstate_suffix		//BBP
		else																													//BBP
			make/o/t/n=1 plotnameS = "dS"+WhichDp+"_sm" + num2str(smpts_Dp) + "_int"+WhichInterp+"_" + Expt + IDstate_suffix									//BBP
		endif																												//BBP
		make/o/n=(numpnts($ScanLims_int_Name[0])) $("dummydS"+WhichDp + "_" + IDstate_suffix+"_int"+WhichInterp+"_sm"+num2str(smpts_Dp))/wave=dummy = 1  // bbp v5.1
		Display/n=$plotnameS[0] dummy vs $ScanLims_int_Name[0];ModifyGraph hideTrace=1;AppendImage $ConcNamez[0] vs {$ScanLims_int_Name[0],Dpb_plot_nm};DelayUpdate	// bbp v5.1
		///set "smart" concentration scale limits based on XXX percentile range of data plotted ("maxImageZ_ptile" from panel)			//BBP
		ModifyImage $ConcNamez[0] ctab= {0,maxImageZ,Rainbow,1}																//BBP
		ColorScale/C/N=text0/A=RC image = $ConcNamez[0]																		//BBP
		ColorScale/C/N=text0 "dSdlogDm (\F'Symbol'm\F'Times New Roman' m\S2\Mcm\S-3\M)"										//BBP
	elseif (stringmatch(PlotType, "Volume"))
		if (max_minutes_interp>=0)
			make/o/t/n=1 plotnameM = "dV"+WhichDp+"_sm" + num2str(smpts_Dp) + "_int"+ WhichInterp + "_" + Expt + IDstate_suffix
		else
			make/o/t/n=1 plotnameM = "dV"+WhichDp+"_sm" + num2str(smpts_Dp) + "_int"+WhichInterp+"_" + Expt + IDstate_suffix
		endif
		make/o/n=(numpnts($ScanLims_int_Name[0])) $("dummydV"+WhichDp + "_" + IDstate_suffix+"_int"+WhichInterp+"_sm"+num2str(smpts_Dp))/wave=dummy = 1  // bbp v5.1
		Display/n=$plotnameM[0] dummy vs $ScanLims_int_Name[0];ModifyGraph hideTrace=1;AppendImage $ConcNamez[0] vs {$ScanLims_int_Name[0],Dpb_plot_nm};DelayUpdate	// bbp v5.1
		///set "smart" concentration scale limits based on XXX percentile range of data plotted ("maxImageZ_ptile" from panel)
		ModifyImage $ConcNamez[0] ctab= {0,maxImageZ,Rainbow,1}
		ColorScale/C/N=text0/A=RC image = $ConcNamez[0]
		ColorScale/C/N=text0 "dVdlogDm (μm\S3\Mcm\S-3\M)"

	endif

	//Apply some universal image plot details
	ModifyGraph log(left)=1;DelayUpdate
	Label left "Dp (nm)"
	Label bottom "\\Z06"
	Label left "\\Z12Dp (nm)\\Z12"
	ModifyGraph margin(right)=120
	ModifyGraph gbRGB=(43690, 43690, 43690)
	TextBox/C/N=text1/F=0/A=MC "ExpID: " + IDstate_suffix

	killwaves/z sortdNdLogDp, maxImageZ, maxImageZ_index, latestgoodEnd, latestgoodStart, lastgoodDur, PlotnameN, PlotnameM, PlotnameCS, PlotnameS, ConcNamez
	killwaves/z NonProcName, smpts_Dp, sortdXdLogDp, tempDate_Time_Plot, temp_Date_Time_Plot, ScanLims_Plot_Name, maxImageZ_ptile, max_minutes_interp, IDstate
	killwaves/z ScanLims_int_Name, dXdLogDp_Plot, dXdLogDp_int, dXdLogDp_int_sm, IDstate_suffix, ScanLims_Plot, ScanLims_int
	killstrings CutInterpNaNs, IDstate_suffix
	setdatafolder $Exptpath
End



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//plot total integrated Number, Surface Area, Cond. Sink, Volume concentration//////////////////////...DD 1/12/2011
function plotTotconc(TotPlotType)

	string TotPlotType

	SVAR ExpFolder = root:temporary:DFNm
	setdatafolder Expfolder
	SMPS_CreateFolder(":SMPS:Plot:")
	SetDataFolder :SMPS:Plot:

	//copy integration limits from root folder (controlled by front panel)
	duplicate/o/d root:Temporary:minInt,  minInt
	duplicate/o/d root:Temporary:maxInt, maxInt

	svar PathFromTempDir = root:temporary:DFNm
	string/g Exptpath = PathFromTempDir
	string Plotdatafolder = Exptpath + "SMPS:Plot:"

	string NoQAfolderNm = Exptpath + "SMPS:NoQA:"
	string RawfolderNm = Exptpath + "SMPS:Raw:"
	string/g Expt = Exptpath[5,strlen(Exptpath)-2]

	NVAR RegridDp = root:Temporary:RegridDp //if zero, use raw data, if 1 use regridded Dp data

	string WhichDp = ""  // bbp v5.1 this string gets inserted into wave and window names later on in this function, depending on Regrid
	if (RegridDp>0)
		WhichDp = ""
		wave Dp_glob = $NoQAfolderNm+"Dp"
		duplicate/o Dp_glob, $("Dp_Plot"+WhichDp)/wave=Dp_Plot
	else
		WhichDp = "Raw"
		wave Dp_glob = $RawfolderNm+"DpRaw"
		duplicate/o Dp_glob, $("Dp_Plot"+WhichDp)/wave=Dp_Plot
	endif						// bbp v5.1 end of new regrid code

	//	if (RegridDp>0)							// bbp v5.1 commented this out and replaced with the code immediately above
	//	wave Dp_glob = $NoQAfolderNm+"Dp"
	//	else
	//	wave Dp_glob = $RawfolderNm+"DpRaw"
	//	endif
	//	duplicate/o Dp_glob, Dp_Plot


	//need to check if all of the files loaded have the same Dp values otherwise should abort if choosing to use the raw data
	wave AllsameMinDp = $RawfolderNm+"AllsameMinDp"
	wave AllsameMaxDp = $RawfolderNm+"AllsameMaxDp"
	if (RegridDp==0 && (AllsameMinDp[0]==0 || AllsameMaxDp[0]==0))
		abort "!!Quitting plotting function b/c Dp ranges are different for some files loaded. Need to use re-gridded data or if want to plot raw (recorded/native) data, reload files with different Dp ranges in different projects."
	endif

	if (stringmatch(TotPlotType, "Number"))
		if (RegridDp>0)
			wave dNdlogDp_Plot_glob = $NoQAfolderNm+"dNdlogDp_Unscaled"
		else
			wave dNdlogDp_Plot_glob = $RawfolderNm+"dNdlogDp"
		endif
		duplicate/o dNdlogDp_Plot_glob, dNdlogDp_Plot
	elseif (stringmatch(TotPlotType, "CondSink"))
		if (RegridDp>0)
			wave dNdlogDp_Plot_glob = $NoQAfolderNm+"dNdlogDp_Unscaled"
		else
			wave dNdlogDp_Plot_glob = $RawfolderNm+"dNdlogDp"
		endif
		duplicate/o dNdlogDp_Plot_glob, dNdlogDp_Plot
	elseif (stringmatch(TotPlotType, "SurfArea"))							//BBP cond sink
		if (RegridDp>0)													//
			wave dSdlogDp_Plot_glob = $NoQAfolderNm+"dSdlogDp_Unscaled"	//
		else																//
			wave dSdlogDp_Plot_glob = $RawfolderNm+"dSdlogDp"				//
		endif															//
		duplicate/o dSdlogDp_Plot_glob, dSdlogDp_Plot						//
	elseif (stringmatch(TotPlotType, "Volume"))
		if (RegridDp>0)
			wave dVdlogDp_Plot_glob = $NoQAfolderNm+"dVdlogDp_Unscaled"
		else
			wave dVdlogDp_Plot_glob = $RawfolderNm+"dVdlogDp"
		endif
		duplicate/o dVdlogDp_Plot_glob, dVdlogDp_Plot
	endif

	wave Date_Time_glob = $RawfolderNm+"Date_Time"
	duplicate/o Date_Time_glob, Date_Time_Plot

	variable minint_met = minint[0]*1e-9
	variable maxint_met = maxint[0]*1e-9

	if (stringmatch(TotPlotType, "Number"))
		make/o/n=(numpnts(Date_Time_Plot)) TotconcN = NaN
		SMPS_dNdlogDp2Total(dNdlogDp_Plot, TotconcN, Dp_Plot, dp_start = minint_met, dp_end=maxint_met)

	elseif (stringmatch(TotPlotType, "CondSink"))																//bbp cond sink
		make/o/n=(numpnts(Date_Time_Plot)) TotconcCS = NaN													//bbp
		wave MW = root:Temporary:MW
		wave diff = root:Temporary:diff
		wave alpha = root:Temporary:alpha
		SVAR ExpFolder = root:temporary:DFNm
		string TempPath_T = ExpFolder+"SMPS:Raw:Temperature"
		wave Temperature = $TempPath_T
		string TempPath_GF = ExpFolder+"SMPS:Raw:Growthfactor"
		wave Growthfactor = $TempPath_GF
		SMPS_dCSdlogDp2Total(MW,Temperature,diff,alpha,dNdlogDp_Plot, TotconcCS, Dp_Plot, dp_start = minint_met, dp_end=maxint_met, GF=growthFactor)	//bbp  for with GF
	elseif (stringmatch(TotPlotType, "SurfArea"))
		make/o/n=(numpnts(Date_Time_Plot)) TotconcS = NaN													//bbp
		SMPS_dNdlogDp2Total(dSdlogDp_Plot, TotconcS, Dp_Plot, dp_start = minint_met, dp_end=maxint_met)		//bbp

	elseif (stringmatch(TotPlotType, "Volume"))
		make/o/n=(numpnts(Date_Time_Plot)) TotconcV = NaN
		SMPS_dNdlogDp2Total(dVdlogDp_Plot, TotconcV, Dp_Plot, dp_start = minint_met, dp_end=maxint_met)
	endif


	//NaN bad data periods
	CutBadDat()
	wave BadStart_SMPS, BadEnd_SMPS
	variable idexx
	for (idexx=0; idexx<numpnts(BadStart_SMPS); idexx+=1)
		if (stringmatch(TotPlotType, "Number"))
			TotconcN = (Date_Time_Plot>=BadStart_SMPS[idexx] && Date_Time_Plot<=BadEnd_SMPS[idexx]) ? NaN : TotconcN
		elseif (stringmatch(TotPlotType, "CondSink"))
			TotconcCS = (Date_Time_Plot>=BadStart_SMPS[idexx] && Date_Time_Plot<=BadEnd_SMPS[idexx]) ? NaN : TotconcCS
		elseif (stringmatch(TotPlotType, "SurfArea"))
			TotconcS = (Date_Time_Plot>=BadStart_SMPS[idexx] && Date_Time_Plot<=BadEnd_SMPS[idexx]) ? NaN : TotconcS
		elseif (stringmatch(TotPlotType, "Volume"))
			TotconcV = (Date_Time_Plot>=BadStart_SMPS[idexx] && Date_Time_Plot<=BadEnd_SMPS[idexx]) ? NaN : TotconcV
		endif
	endfor


	///////////////////////////////////--ExpID State Filtering--////////////////////////////////////////////////
	//pair down time wave and concentration matrix to specific choosen ExpID state
	if (waveexists(ExpIDSMPS))
		//do nothing
	else
		if (exists("root:Masterlogger:ExpID"))
			ExpID2SMPS() //first run function to pick out ExpID states for SMPS scan times.
		else
			make/o/n=(numpnts(Date_Time_Plot)) ExpIDSMPS = 99
		endif
	endif

	wave ExpIDSMPS
	//wave root:Temporary:IDState
	NVAR IDState = root:Temporary:IDState
	if (IDState== 99)
		make/o/t/n=1 TotnameN = "TotconcN"+WhichDp+"_all"
		make/o/t/n=1 TotnameV = "TotconcV"+WhichDp+"_all"
		make/o/t/n=1 TotnameCS = "TotconcCS"+WhichDp+"_all"	  //bbp cond sink
		make/o/t/n=1 TotnameS = "TotconcS"+WhichDp+"_all"	  //bbp surf area
		make/o/t/n=1 Totname_time = "TotTime_all"
		//do nothing - i.e. keep all data
		//string/g IDState_name = "All States"
		print "Will Plot All States"
		if (stringmatch(TotPlotType, "Number"))
			duplicate/o TotconcN, $TotnameN[0]
		elseif (stringmatch(TotPlotType, "Volume"))
			duplicate/o TotconcV, $TotnameV[0]
		elseif (stringmatch(TotPlotType, "CondSink")) 	//bbp cond sink
			duplicate/o TotconcCS, $TotnameCS[0]		//bbp
		elseif (stringmatch(TotPlotType, "Surfarea")) 	//bbp surfarea
			duplicate/o TotconcS, $TotnameS[0]		//bbp
		endif
		duplicate/o Date_Time_Plot, TotTime_all
	else
		make/o/t/n=1 TotnameN = "TotconcN"+WhichDp+"_" + num2str(IDState)
		make/o/t/n=1 TotnameV = "TotconcV"+WhichDp+"_" + num2str(IDState)
		make/o/t/n=1 TotnameCS = "TotconcCS"+WhichDp+"_" + num2str(IDState)
		make/o/t/n=1 TotnameS = "TotconcS"+WhichDp+"_" + num2str(IDState)
		make/o/t/n=1 Totname_time = "TotTime_" + num2str(IDState)

		wave/t FlagNames
		//string/g IDState_name =  FlagNames[IDState]
		if (stringmatch(TotPlotType, "Number"))
			extract TotconcN, $TotnameN[0], (ExpIDSMPS==IDState)
		elseif (stringmatch(TotPlotType, "Volume"))
			extract TotconcV, $TotnameV[0], (ExpIDSMPS==IDState)
		elseif (stringmatch(TotPlotType, "CondSink"))				//bbp cond sink
			extract TotconcCS, $TotnameCS[0], (ExpIDSMPS==IDState)	//
		elseif (stringmatch(TotPlotType, "Surfarea"))				//bbp surf area sink
			extract TotconcS, $TotnameS[0], (ExpIDSMPS==IDState)	//  bbp surf area
		endif
		extract Date_Time_Plot, $Totname_time[0], (ExpIDSMPS==IDState)
		//    print "in "+ FlagNames[IDState] + "-loop"
	endif

	////////////total integrated Number concentration plot//////////////////////
	if (stringmatch(TotPlotType, "Number"))
		wave totConc
		Display/N= $TotnameN[0]  $TotnameN[0] vs $Totname_time[0] as "TotalNum: " +  num2str(minint[0])  +  "-"  + num2str(maxint[0]) + "nm"+ " " + Expt
		SetScale d 0,0,"dat", Date_Time_Plot
		ModifyGraph mirror(left)=1,mirror(bottom)=2
		Label left "cm\\S-3"
		Legend/C/N=text0/J/A=LT "Integrated SMPS Number Concentration (" + num2str(minint[0]) + "nm -  " +  num2str(maxint[0]) + "nm only)"

		////////////total integrated Volume concentration plot//////////////////
	elseif (stringmatch(TotPlotType, "Volume"))
		wave totConc
		Display/N= $TotnameV[0]  $TotnameV[0] vs $Totname_time[0] as "TotalVol: " +  num2str(minint[0])  +  "-"  + num2str(maxint[0]) + "nm"+ " " + Expt
		SetScale d 0,0,"dat", Date_Time_Plot
		ModifyGraph mirror(left)=1,mirror(bottom)=2
		Label left "cm\\S-3"
		Legend/C/N=text0/J/A=LT "Integrated SMPS Volume Concentration (" + num2str(minint[0]) + "nm -  " +  num2str(maxint[0]) + "nm only)"
		ModifyGraph rgb=(16384,16384,65280)
		Label left "μm\S3\Mcm\S-3"

		////////////total integrated condensational sink concentration plot//////////////////////  //BBP cond sink
	elseif (stringmatch(TotPlotType, "CondSink"))
		wave totConc
		Display/N= $TotnameCS[0]  $TotnameCS[0] vs $Totname_time[0] as "TotalCS: " +  num2str(minint[0])  +  "-"  + num2str(maxint[0]) + "nm"+ " " + Expt
		SetScale d 0,0,"dat", Date_Time_Plot
		ModifyGraph mirror(left)=1,mirror(bottom)=2
		Label left "s\\S-1"
		Legend/C/N=text0/J/A=LT "Integrated SMPS Cond. Sink Concentration (" + num2str(minint[0]) + "nm -  " +  num2str(maxint[0]) + "nm only)"
		ModifyGraph rgb=(44032,29440,58880)

		////////////total integrated Surface area concentration plot//////////////////////  //BBP surf area
	elseif (stringmatch(TotPlotType, "Surfarea"))
		wave totConc
		Display/N= $TotnameS[0]  $TotnameS[0] vs $Totname_time[0] as "TotalS: " +  num2str(minint[0])  +  "-"  + num2str(maxint[0]) + "nm"+ " " + Expt
		SetScale d 0,0,"dat", Date_Time_Plot
		ModifyGraph mirror(left)=1,mirror(bottom)=2
		Label left "\F'Symbol'm\F'Times New Roman'm\\S2\\M cm\\S-3"
		Legend/C/N=text0/J/A=LT "Integrated SMPS Surf. Area Concentration (" + num2str(minint[0]) + "nm -  " +  num2str(maxint[0]) + "nm only)"
		ModifyGraph rgb=(0,39168,0)
	endif

	SetScale d 0,0,"dat", $Totname_time[0] //So plots as time/date format, not integer
	Label bottom " "
	if (IDState== 99)
		TextBox/C/N=text1/F=0/A=MC "ExpID: All"
	else
		TextBox/C/N=text1/F=0/A=MC "ExpID: " + num2str(IDState)
	endif
	killwaves/z TotnameN, TotnameS, TotnameV, TotnameCS, Totname_time, maxInt, maxint_met, minInt, minint_met, IDState, dNdlogDp_Plot, dVdlogDp_Plot
	//killstrings IDState_name
	setdatafolder $Exptpath
End


///////////////////PANEL BUTTONS////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

//call up Mike's loading/concatenation functions
Function LoadConcatenate(ctrlName) : ButtonControl
	String ctrlName
	JG_SMPSImportDir()
End

//plots up size distribution time series as image plot
Function PlotNumDistr_Button(ctrlName) : ButtonControl
	String ctrlName
	plotImage("Number")
End

//plots up size distribution time series as image plot
Function PlotSurfAreaDistr_Button(ctrlName) : ButtonControl							//BBP surf area
	String ctrlName																//BBP
	plotImage("SurfArea") 	//BBP
End																				//BBP

//plots up CS distribution time series as image plot
Function PlotCSDistr_Button(ctrlName) : ButtonControl							//BBP CS
	String ctrlName																//BBP
	plotImage("CondSink") 	//BBP
End

//plots up size distribution time series as image plot
Function PlotVolDistr_Button(ctrlName) : ButtonControl
	String ctrlName
	plotImage("Volume")
End

//plots up size distribution time series as image plot
Function PlotTotNum_Button(ctrlName) : ButtonControl
	String ctrlName
	plotTotconc("Number")
End

//plots up CS integrated	//BBP
Function PlotTotCS_Button(ctrlName) : ButtonControl
	String ctrlName
	plotTotconc("CondSink")
End

//plots up Surface Area integrated	//BBP
Function PlotTotS_Button(ctrlName) : ButtonControl
	String ctrlName
	plotTotconc("SurfArea")
End

//plots up size distribution time series as image plot
Function PlotTotVol_Button(ctrlName) : ButtonControl
	String ctrlName
	plotTotconc("Volume")
End

function CS_paramSetup(ctrlName) : ButtonControl
	String ctrlName
	SVAR ExpFolder = root:temporary:DFNm
	setdatafolder Expfolder
	wave MW = root:temporary:MW
	wave Temperature = $(ExpFolder+"SMPS:Raw:Temperature")
	wave Diff = root:temporary:Diff
	wave Alpha = root:temporary:Alpha
	wave Growthfactor = $(ExpFolder+"SMPS:Raw:Growthfactor")
	SVAR ExptName = root:Temporary:ExptName
	dowindow/K CSparams
	edit/N=CSparams MW,Temperature,Diff,Alpha,Growthfactor as "CSparams for project: " + ExptName
	print "User should redefine Condensational Sink inputs (single variables or time-dependant) now if desired before making plots"
end


///////////////////make SMPS Plotting PANEL ////////////////////////////////////////
//master function that runs the panel populate then makes the panel
Function SMPSpanel()
	dowindow/F SMPS_Plotting
	if (V_flag == 0) //if window doesn't already exist then regenerate panel
		PopulateSMPSpanel()
		Execute "SMPS_Plotting()"
	else
		//window exists and had been brought to front (don't regenerate)
	endif
End

/////////////////////////////////////////////////////////////////////////////////////////////
//First create Panel Variables if they don't exist
Function PopulateSMPSpanel()

	if(waveexists($("root:Temporary:IDState")))  // bbp v5.1 if IDState isn't there but the temporary folder is, then you're updating an old pxp from <=v4 to v5 and you need to kill the temporary folder so that it remakes the updated waves/variables in that folder
		// do nothing
	else
		killdatafolder/Z root:Temporary
	endif

	if(datafolderexists("root:Temporary"))
		//do nothing
	else //create variables needed for panel creation
		newdatafolder root:Temporary
		make/o/n=1 root:Temporary:maxInt = 1000
		make/o/n=1 root:Temporary:minInt = 0
		variable/G root:Temporary:max_minutes_interp = 15
		variable/G root:Temporary:maxImageZ_ptile = 95
		variable/G root:Temporary:smpts_Dp = 0
		variable/G root:Temporary:IDState = 99
		variable/G root:Temporary:AIM_ExportVersion = 11
		variable/G root:Temporary:RegridDp = 1
		variable/G root:Temporary:RegridDp_NumBins = 128


		string/g root:Temporary:DFNm = "root:CHOOSE FOLDER:"
		//AssignFlags()
		//Assign default values for Cond. Sink Calcs (single point) with temperature and growthfactor to be expanded to SMPS timestamp in functions later
		make/o/n=1  root:Temporary:MW = 200 //g/mol; Palm et al., ACP, 2016
		make/o/n=1  root:Temporary:Temperature = 298 //Kelvin;
		make/o/n=1  root:Temporary:Diff = 7e-2 //cm2/s; Palm et al., ACP, 2016 for ~MW ~200 molecule
		make/o/n=1  root:Temporary:Alpha = 1 //sticking coefficient
		make/o/n=1  root:Temporary:Growthfactor = 1 //for converting meaured dried size to actual humidfied conditions
	endif
End

//Will need some updating later b/c "AssignFlags" and specific lines drawing FlagNamesString text will get removed if SMPS plotting panel is replaced
Window SMPS_Plotting() : Panel
	PauseUpdate; Silent 1		// building window...
	NewPanel /W=(825,304,1281,822) as "SMPS Plotting"
	ModifyPanel fixedSize=1, frameStyle=3
	SetDrawLayer UserBack
	SetDrawEnv fillfgc= (65535,54611,49151),fillbgc= (65535,54611,49151)
	DrawRRect 18,217,435,387
	SetDrawEnv fillfgc= (52224,52224,52224)
	DrawRect 68,5,431,103
	SetDrawEnv fillfgc= (48896,65280,65280)
	DrawRRect 16,392,434,497
	SetDrawEnv fsize= 16,fstyle= 1
	DrawText 139,30,"Load-Concatenate SMPS Data"
	SetDrawEnv linefgc= (65535,0,0),fsize= 18,fstyle= 1,textrgb= (65280,21760,0)
	DrawText 30,246,"Image Time Series Plots"
	SetDrawEnv fsize= 18,fstyle= 1,textrgb= (0,0,65280)
	DrawText 25,419,"Integrated Time Series Plots"
	SetDrawEnv fsize= 9
	DrawText 101,375,"(Insert zero for no interpolation = insert nans at all gaps)"
	SetDrawEnv fsize= 9
	DrawText 15,513,"Doug Day, Seonsik Yun, Brett Palm, Mike Cubison, CU Boulder"
	DrawText 94,442,"Limit Integration Size Range"
	SetDrawEnv fsize= 9
	DrawText 89,44,"Input datafiles must be tab-delimited rows and dNdLogDp (see ipf header) "
	SetDrawEnv fsize= 9
	DrawText 286,513,"JG_SMPS version 6.10, 2023.11.09"
	DrawPICT 4,5,1,1,JG_SMPS#CUlogoNew
	SetDrawEnv fsize= 9
	DrawText 100,386,"(Insert negative for never inserting nans. Best for export data)"
	SetDrawEnv fsize= 9
	DrawText 46,138,"For filtering, put waves \"ExpID\", \"t_start\", \"t_stop\" in \"root:MasterLogger\" datafolder"
	SetDrawEnv fsize= 9
	DrawText 87,152,"Delete \"ExpIDSMPS\" in Project/SMPS/Plot folder before updating"
	SetDrawEnv fsize= 9
	DrawText 57,320,"(above assigned hottest color, inhibits "
	SetDrawEnv fsize= 9
	DrawText 62,334,"outliers from collapsing color range) "
	SetDrawEnv fsize= 9
	DrawText 57,213," (Use when imported data has different Dp ranges. See warning in ipf header.)"
	SetDrawEnv fsize= 9
	DrawText 38,481,"(automatically initialized to max/min for project selected)"
	SetDrawEnv fsize= 9
	DrawText 312,100,"(~64 recomended)"
	SetDrawEnv fsize= 9
	DrawText 159,100,"(done during load)"
	Button Load_Concatenate,pos={308.00,45.60},size={109.60,21.60},proc=LoadConcatenate
	Button Load_Concatenate,title="Load-Concatenate"
	Button Load_Concatenate,userdata(ResizeControlsInfo)=A"!!,HO!!#>:!!#@n!!#<hz!!#](Aon\"Qzzzzzzzzzzzzzz!!#](Aon\"Qzz"
	Button Load_Concatenate,userdata(ResizeControlsInfo)+=A"zzzzzzzzzzzz!!#u:Du]k<zzzzzzzzzzz"
	Button Load_Concatenate,userdata(ResizeControlsInfo)+=A"zzz!!#u:Du]k<zzzzzzzzzzzzzz!!!"
	Button Load_Concatenate,fColor=(65535,65535,65535)
	Button Plot_Integrated_Concentration,pos={281.60,393.60},size={140.00,21.60},proc=PlotTotNum_Button
	Button Plot_Integrated_Concentration,title="Plot Integrated Number"
	Button Plot_Integrated_Concentration,userdata(ResizeControlsInfo)=A"!!,HLJ,hs]J,hq=!!#<hz!!#](Aon\"Qzzzzzzzzzzzzzz!!#](Aon\"Qzz"
	Button Plot_Integrated_Concentration,userdata(ResizeControlsInfo)+=A"zzzzzzzzzzzz!!#u:Du]k<zzzzzzzzzzz"
	Button Plot_Integrated_Concentration,userdata(ResizeControlsInfo)+=A"zzz!!#u:Du]k<zzzzzzzzzzzzzz!!!"
	Button Plot_Integrated_Concentration,fColor=(65280,0,0)
	Button Plot_Integrated_CS,pos={281.60,417.60},size={69.60,21.60},proc=CS_paramSetup
	Button Plot_Integrated_CS,title="CS params",fColor=(36864,14592,58880)
	Button Plot_Integrated_CS2,pos={353.60,417.60},size={69.60,21.60},proc=PlotTotCS_Button
	Button Plot_Integrated_CS2,title="Int. CS RC",fColor=(13056,0,26112)
	Button Plot_Integrated_S,pos={281.60,444.00},size={141.60,21.60},proc=PlotTotS_Button
	Button Plot_Integrated_S,title="Plot Integrated Surf. Area"
	Button Plot_Integrated_S,fColor=(0,32224,15000)
	Button image_plot,pos={257.60,221.60},size={169.60,28.00},proc=PlotNumDistr_Button
	Button image_plot,title="Plot Number Distributions"
	Button image_plot,userdata(ResizeControlsInfo)=A"!!,HA!!#B>!!#@o!!#=Cz!!#](Aon\"Qzzzzzzzzzzzzzz!!#](Aon\"Qzz"
	Button image_plot,userdata(ResizeControlsInfo)+=A"zzzzzzzzzzzz!!#u:Du]k<zzzzzzzzzzz"
	Button image_plot,userdata(ResizeControlsInfo)+=A"zzz!!#u:Du]k<zzzzzzzzzzzzzz!!!"
	Button image_plot,fColor=(65280,0,0)
	Button image_plot2,pos={257.60,285.60},size={169.60,28.00},proc=PlotSurfAreaDistr_Button
	Button image_plot2,title="Plot Surface Area Distributions"
	Button image_plot2,userdata(ResizeControlsInfo)=A"!!,HA!!#B>!!#@o!!#=Cz!!#](Aon\"Qzzzzzzzzzzzzzz!!#](Aon\"Qzz"
	Button image_plot2,userdata(ResizeControlsInfo)+=A"zzzzzzzzzzzz!!#u:Du]k<zzzzzzzzzzz"
	Button image_plot2,userdata(ResizeControlsInfo)+=A"zzz!!#u:Du]k<zzzzzzzzzzzzzz!!!"
	Button image_plot2,fColor=(0,13056,6400)
	Button image_plot3,pos={316.00,253.60},size={109.60,28.00},proc=PlotCSDistr_Button
	Button image_plot3,title="CS Rate Const. Dist."
	Button image_plot3,userdata(ResizeControlsInfo)=A"!!,HA!!#B>!!#@o!!#=Cz!!#](Aon\"Qzzzzzzzzzzzzzz!!#](Aon\"Qzz"
	Button image_plot3,userdata(ResizeControlsInfo)+=A"zzzzzzzzzzzz!!#u:Du]k<zzzzzzzzzzz"
	Button image_plot3,userdata(ResizeControlsInfo)+=A"zzz!!#u:Du]k<zzzzzzzzzzzzzz!!!"
	Button image_plot3,fColor=(19712,0,39168)
	SetVariable Pts_to_smooth_Dp_imageplot,pos={25.60,257.60},size={220.00,18.40}
	SetVariable Pts_to_smooth_Dp_imageplot,title="Pts to smooth Dp (<3 no smooth)"
	SetVariable Pts_to_smooth_Dp_imageplot,userdata(ResizeControlsInfo)=A"!!,Ct!!#B@J,hr-!!#<8z!!#](Aon\"Qzzzzzzzzzzzzzz!!#](Aon\"Qzz"
	SetVariable Pts_to_smooth_Dp_imageplot,userdata(ResizeControlsInfo)+=A"zzzzzzzzzzzz!!#u:Du]k<zzzzzzzzzzz"
	SetVariable Pts_to_smooth_Dp_imageplot,userdata(ResizeControlsInfo)+=A"zzz!!#u:Du]k<zzzzzzzzzzzzzz!!!"
	SetVariable Pts_to_smooth_Dp_imageplot,limits={0,inf,1},value=root:Temporary:smpts_Dp
	SetVariable maxDatagap_interp,pos={24.00,345.60},size={404.00,18.40}
	SetVariable maxDatagap_interp,title="Maximum Data Gap - minutes (< interpolate; > insert Nan, plot blank"
	SetVariable maxDatagap_interp,userdata(ResizeControlsInfo)=A"!!,Cd!!#BdJ,hsC!!#<8z!!#](Aon\"Qzzzzzzzzzzzzzz!!#](Aon\"Qzz"
	SetVariable maxDatagap_interp,userdata(ResizeControlsInfo)+=A"zzzzzzzzzzzz!!#u:Du]k<zzzzzzzzzzz"
	SetVariable maxDatagap_interp,userdata(ResizeControlsInfo)+=A"zzz!!#u:Du]k<zzzzzzzzzzzzzz!!!"
	SetVariable maxDatagap_interp,value=root:Temporary:max_minutes_interp
	SetVariable minimum_bin_to_integrate_N_V,pos={52.00,445.60},size={96.00,18.40}
	SetVariable minimum_bin_to_integrate_N_V,title="min (nm)"
	SetVariable minimum_bin_to_integrate_N_V,userdata(ResizeControlsInfo)=A"!!,E2!!#CDJ,hpA!!#<8z!!#](Aon\"Qzzzzzzzzzzzzzz!!#](Aon\"Qzz"
	SetVariable minimum_bin_to_integrate_N_V,userdata(ResizeControlsInfo)+=A"zzzzzzzzzzzz!!#u:Du]k<zzzzzzzzzzz"
	SetVariable minimum_bin_to_integrate_N_V,userdata(ResizeControlsInfo)+=A"zzz!!#u:Du]k<zzzzzzzzzzzzzz!!!"
	SetVariable minimum_bin_to_integrate_N_V,value=root:Temporary:minInt[0]
	SetVariable maximum_bin_to_integrate_N_V,pos={153.60,445.60},size={108.00,18.40}
	SetVariable maximum_bin_to_integrate_N_V,title="max (nm)"
	SetVariable maximum_bin_to_integrate_N_V,userdata(ResizeControlsInfo)=A"!!,G*!!#CDJ,hpS!!#<8z!!#](Aon\"Qzzzzzzzzzzzzzz!!#](Aon\"Qzz"
	SetVariable maximum_bin_to_integrate_N_V,userdata(ResizeControlsInfo)+=A"zzzzzzzzzzzz!!#u:Du]k<zzzzzzzzzzz"
	SetVariable maximum_bin_to_integrate_N_V,userdata(ResizeControlsInfo)+=A"zzz!!#u:Du]k<zzzzzzzzzzzzzz!!!"
	SetVariable maximum_bin_to_integrate_N_V,value=root:Temporary:maxInt[0]
	PopupMenu dataDFSel1,pos={31.20,157.60},size={357.60,24.00},bodyWidth=125,proc=pmfCalcs_popMenu_DataFolder
	PopupMenu dataDFSel1,title="Select \"Project\" Data Folder Before Plotting"
	PopupMenu dataDFSel1,userdata(ResizeControlsInfo)=A"!!,Dg!!#AD!!#Ba!!#<`z!!#](Aon\"Qzzzzzzzzzzzzzz!!#](Aon\"Qzz"
	PopupMenu dataDFSel1,userdata(ResizeControlsInfo)+=A"zzzzzzzzzzzz!!#u:Du]k<zzzzzzzzzzz"
	PopupMenu dataDFSel1,userdata(ResizeControlsInfo)+=A"zzz!!#u:Du]k<zzzzzzzzzzzzzz!!!"
	PopupMenu dataDFSel1,fSize=12
	PopupMenu dataDFSel1,mode=7,popvalue="Test2:",value=#"gen_dataFolderList_wCurrDF(\"root:\")"
	Button image_plot1,pos={256.00,316.00},size={169.60,28.00},proc=PlotVolDistr_Button
	Button image_plot1,title="Plot Volume Distributions"
	Button image_plot1,userdata(ResizeControlsInfo)=A"!!,H;J,hs&J,hq[!!#=Cz!!#](Aon\"Qzzzzzzzzzzzzzz!!#](Aon\"Qzz"
	Button image_plot1,userdata(ResizeControlsInfo)+=A"zzzzzzzzzzzz!!#u:Du]k<zzzzzzzzzzz"
	Button image_plot1,userdata(ResizeControlsInfo)+=A"zzz!!#u:Du]k<zzzzzzzzzzzzzz!!!"
	Button image_plot1,fColor=(0,0,52224)
	Button Plot_Integrated_Concentration1,pos={281.60,468.00},size={140.00,21.60},proc=PlotTotVol_Button
	Button Plot_Integrated_Concentration1,title="Plot Integrated Volume"
	Button Plot_Integrated_Concentration1,userdata(ResizeControlsInfo)=A"!!,HM!!#CE!!#@g!!#<hz!!#](Aon\"Qzzzzzzzzzzzzzz!!#](Aon\"Qzz"
	Button Plot_Integrated_Concentration1,userdata(ResizeControlsInfo)+=A"zzzzzzzzzzzz!!#u:Du]k<zzzzzzzzzzz"
	Button Plot_Integrated_Concentration1,userdata(ResizeControlsInfo)+=A"zzz!!#u:Du]k<zzzzzzzzzzzzzz!!!"
	Button Plot_Integrated_Concentration1,fColor=(0,0,52224)
	SetVariable IDState,pos={152.00,105.60},size={172.00,18.40}
	SetVariable IDState,title="Select ExpID: 99=All Data"
	SetVariable IDState,userdata(ResizeControlsInfo)=A"!!,E*!!#@.!!#BV!!#<8z!!#](Aon\"Qzzzzzzzzzzzzzz!!#](Aon\"Qzz"
	SetVariable IDState,userdata(ResizeControlsInfo)+=A"zzzzzzzzzzzz!!#u:Du]k<zzzzzzzzzzz"
	SetVariable IDState,userdata(ResizeControlsInfo)+=A"zzz!!#u:Du]k<zzzzzzzzzzzzzz!!!"
	SetVariable IDState,valueBackColor=(65280,65280,0),value=root:Temporary:IDState
	SetVariable AIM_ExportVersion,pos={72.00,48.00},size={233.60,18.40}
	SetVariable AIM_ExportVersion,title="AIM Version Used to Export Text File"
	SetVariable AIM_ExportVersion,userdata(ResizeControlsInfo)=A"!!,E^!!#>F!!#A]!!#<8z!!#](Aon\"Qzzzzzzzzzzzzzz!!#](Aon\"Qzz"
	SetVariable AIM_ExportVersion,userdata(ResizeControlsInfo)+=A"zzzzzzzzzzzz!!#u:Du]k<zzzzzzzzzzz"
	SetVariable AIM_ExportVersion,userdata(ResizeControlsInfo)+=A"zzz!!#u:Du]k<zzzzzzzzzzzzzz!!!"
	SetVariable AIM_ExportVersion,valueBackColor=(65280,65280,0)
	SetVariable AIM_ExportVersion,limits={4,11,1},value=root:Temporary:AIM_ExportVersion
	SetVariable RegridDp,pos={72.00,180.00},size={253.60,18.40}
	SetVariable RegridDp,title="Use Regrid to common Dp, 0=no, 1=yes:"
	SetVariable RegridDp,userdata(ResizeControlsInfo)=A"!!,B9!!#A^!!#Ao!!#<8z!!#](Aon\"Qzzzzzzzzzzzzzz!!#](Aon\"Qzz"
	SetVariable RegridDp,userdata(ResizeControlsInfo)+=A"zzzzzzzzzzzz!!#u:Du]k<zzzzzzzzzzz"
	SetVariable RegridDp,userdata(ResizeControlsInfo)+=A"zzz!!#u:Du]k<zzzzzzzzzzzzzz!!!"
	SetVariable RegridDp,valueBackColor=(65280,65280,0)
	SetVariable RegridDp,limits={0,1,1},value=root:Temporary:RegridDp
	SetVariable RegridDp_NumBins,pos={73.60,68.00},size={313.60,18.40}
	SetVariable RegridDp_NumBins,title="Number of bins/decade to regrid for common Dp"
	SetVariable RegridDp_NumBins,userdata(ResizeControlsInfo)=A"!!,Eb!!#?E!!#BW!!#<8z!!#](Aon\"Qzzzzzzzzzzzzzz!!#](Aon\"Qzz"
	SetVariable RegridDp_NumBins,userdata(ResizeControlsInfo)+=A"zzzzzzzzzzzz!!#u:Du]k<zzzzzzzzzzz"
	SetVariable RegridDp_NumBins,userdata(ResizeControlsInfo)+=A"zzz!!#u:Du]k<zzzzzzzzzzzzzz!!!"
	SetVariable RegridDp_NumBins,fSize=12,valueBackColor=(65280,65280,0)
	SetVariable RegridDp_NumBins,value=root:Temporary:RegridDp_NumBins
	SetVariable Pts_to_smooth_Dp_imageplot_1,pos={45.60,280.00},size={180.00,18.40}
	SetVariable Pts_to_smooth_Dp_imageplot_1,title="Max Percentile dXdLogDp"
	SetVariable Pts_to_smooth_Dp_imageplot_1,userdata(ResizeControlsInfo)=A"!!,E\"!!#BKJ,hqX!!#<8z!!#](Aon\"Qzzzzzzzzzzzzzz!!#](Aon\"Qzz"
	SetVariable Pts_to_smooth_Dp_imageplot_1,userdata(ResizeControlsInfo)+=A"zzzzzzzzzzzz!!#u:Du]k<zzzzzzzzzzz"
	SetVariable Pts_to_smooth_Dp_imageplot_1,userdata(ResizeControlsInfo)+=A"zzz!!#u:Du]k<zzzzzzzzzzzzzz!!!"
	SetVariable Pts_to_smooth_Dp_imageplot_1,value=root:Temporary:maxImageZ_ptile
	Button Plot_Integrated_CS1,pos={257.60,253.60},size={53.60,28.00},proc=CS_paramSetup
	Button Plot_Integrated_CS1,title="CS params",fSize=10,fColor=(36864,14592,58880)
	SetWindow kwTopWin,userdata(ResizeControlsInfo)=A"!!*'\"z!!#CJ!!#Cf!!!!\"zzzzzzzzzzzzzzzzzzzz"
	SetWindow kwTopWin,userdata(ResizeControlsInfo)+=A"zzzzzzzzzzzzzzzzzzzzzzzzz"
	SetWindow kwTopWin,userdata(ResizeControlsInfo)+=A"zzzzzzzzzzzzzzzzzzz!!!"
EndMacro

//^New one has white background, approx same size as original
// PNG: width= 99, height= 68
static Picture CUlogoNew
ASCII85Begin
M,6r;%14!\!!!!.8Ou6I!!!!p!!!!W#Qau+!""!NfDkmO$#iF<ErZ1M_Z0ZH_Z@erW+5l.(i`,85u`
*!m@&<BSCNbcX1Q..hYSn<8Fjb=5B_(ebTmTg30T\K800$0)+'j$j5$"Pac\3uQ5@Z_$T9^bKI%n!"
EFbKN:A*kpJkr[/[[.OP/1YDL4KnT%![W2`@Z4^9>*R$-/0Qs5fh.k06QQ@mZu/3p\3M@<Rn3$^b8H
)6k*!\eBottaShPF<?9I,1!K_$#>@g*30M;2#bDpRU$RQ9VD0r[R5ABm5gr+X&WLk]*1P2<e.=Dq2U
SE6p]p3LVp25eNZsLBWfd6oA-@[CcN<<E*9u.`&_'9j^R,5&;Po$-3KHeUc1,dWm=1(<9X:0knhd5l
30M8n%8eoYf?h`*&*"r#-Vumbq?\/@!71QHchms%5=#/MC)Z@Te5P&@A>3[rK4KoBIg5^XBoTA\4bk
qnHM1^i!Q5L)BD*^i[4T,8`eLe/VkVkVXKeHMX0)B_c,!D9D^(c<1P7$C-L,jLW-j!4cQ[il=u9+*1
_[:'kM%sNEb8ZKbKlUYeK'eLRsZ0f]@ZCoY5I5`I^ih2\"A(`nZu"Cp=Ij5r0pkVePYeXf%@Ep+13>
bb.YB]A,\(Gqk@d:cRo]+Lo4)L5jRmfqYOG_Rp%"9M:d\TGP,GbB0.P5"Ipr$'HfLt'Q>In8mn"%`E
DoI\2"\#)Jq%uql:>/S/FG,kTbgGJnUOKIWiSJG5Gb>`S9J/WA@5d'Bu2L>/Qh6JnnSBCKn^Ns7V;[
?^_B5#)WBo-b(F)&4?niGXWpCEKkHpBQ:UoR@>`-I-GBjqKF`jU$?l9e'YYi&I/mQ&N*k?S43AuUeN
(oN7eY=)u-Wr&c#.BTQ^2g-/5%@\a_/0!4_BZR$)BO7E+M!P+CX4+A.lt2NHEL,)DE"=TAN%MT\9m7
'M./hnH`^qQOnYgp[d&]lggo;*2D!s#maO\J'sN0lY&NcnJrRAk(4)rLOg+ltbDSU[R'TCqskKa)&s
,.U`Z1$KG,uE=gtm":)?f'oQh9W-/PN_'V]@i[C(Zkb+ZO9%JTA#Y0Bl)L/>@nCY@VqD%T(M400e@I
-eM8J<m,)S2o9W0oZ<Ug_i(I6k50egm9R(ecd60ML!E-CQ0Ire'USMAMo_cDEJVgf/X`VV4*Rh"ZNH
lgEJIs7`k2Fo;iZY.J=^mCc_>mk%m%rN]"6fd,L#!8o7?.?u;Y#U))9ln5O*J`k#6Sema6G-=X?[1<
jl'6OsEqH3o4)JJ9!F$W%g&)'ZL0"r22'sic%@bO5Fs7!bc3[;i-ofqf&/Ehp)pI6[=g>OXK$C9g21
92r%PFh8l4e[/79dSUi2;LtaO)CG&(P,GD;Mc%:jd^"l#^3hN8V0;4LBs`_^RC*G(@Y9N)BV(hi7ZW
MG\6FS1f'j.6JXo_5X";qa*j^I&$;E`lW4NOpG&Ed@LDYDi6as0\#Ru"?_Ut@dT`1401Z<0ISsD+$^
/4MY==k%H"QLQc0=^6G5)FiHWJDdr_5/%m+tX7;PI'`:b(.DcH^J@%Q:N([k5%o0i6L:/8>M-[5Mme
BRUAL6`;(qr@$q5BVP2(d\;Rf2B"M34[">8!#brqrJ9p3kuZV,>Il%04+m.Y)A[Pnf!]hS[Wki6BsW
.d<kFimCp&+=^9dZ$BR+7.XPRnnDn)ab>P6`X/s$rI8h$$GbZ(<9+uVo,bUM3KJ:%YK$VXN-H-%'-s
+YL^rg81&GMA>;D,2@iNG+[q0\;q\(/<;d1u^7.59G5*G3lnE1B)N&o<6ts2E!F/k_mu5#7B5@KcP_
%:'">nXC`!=6Nk"(:'so:oE8oTK1e8\F$&/u'E49)(#P<+:b#?mdW7gakGe/;X%cQL/:pk'U=a!(c$
)2,L2/fqY4[dGFcDu8c+]0NS@XepqE2+hRYVmAi5+7d6M5-R^8e]H.M8]Up#?FoFaJ[SJ_8/hgZoY7
H"K#2McBqJ9L#MlK-\@b';7@rBs4]okpoI($6qX!II/@YVH-;Xb4p#m3%JrKC:DBL;\bf\i&Qfr>Uf
`u;sXpcD#V[jo5@BWf'-fWUH.nupXV_Q;`\YSC79liqCse)TpEQH@FU$l^CtCBY1W>danP57id5lT_
-'&8^`a9p%o)_.Sc4cfs+6:#(7WC8QVDI+2?a4+7'N_tUk1D[_TQ2U9>0YObM,?0$#`72?"Hr?AQ)u
rBE=%4Mj1QKe1U?M_E49N4jQP6bpoP&,>r^&#nmVF#SS*;AKo8br9?+?UOO$Da&#,j7mo_fN/V%A/k
0UpW9IfYbTY2XTVW-lnJ\;!*W<14TfprT^EH02-o+Y1,4Kl2.P<$^J3p??Yes\0#SfMs6qNun!B'os
9Uhp730jF5[J#*\flGQL+;/(V3pXgnJ,]36gb5b^n]\!Rc`HunUC/G07BjZ$kuU[\qW2pKrJE[ST5s
h\-EH\sB0->l'g^gcUK#I8@QnFULq!;$E]]_Y@2OfhBkX,r2Jo1[7Pk@t*'F4,UbTA;*j+8)X""`fl
E4LXhRRZa6R<jp<X38[Lia8<eB+60Xs*?3`Hq\[g&f5nMne?biKbGAQ5a7No7j'EVS.FYB:u=V[XnU
#aHt#%(H@fG&/5-Z!?;_G<:,PP3b)/]Is"P#rSC@^>@d-G=*$.YABKm!93d/?7"VK@G=^"+s8<tsc,
0X566&9IA""a)3f]-Hi^qWU3-5:YN[2:'qq>(8]Qs@+)e[d*^%3$Nc(B:c'0qhMM\eB[jG:]?,Z"eO
39.(:@MR^M-&GI;[r.b(0>kF?=3[4S<El-+a&h!D:mBW2UX$Z&QXo'WMI"Ki78=D*8eal3L%--JJIP
q"=jYPMX2huNc1@./YOG.!AAZNC+-/=P0Th<35-s?F]+4ac(3d2MNHbfT1nGB:?C^^Ve.GK+KVq%LF
$Hlf](1@.$C<ii8Hf,SLZG71,)d=K&7c>O:n1S+>CWu]E,T,UBDoK7f(BLSGq+)QbNt"mb`6t7[_%H
EK1gPXoCg2HSJ!n^OUrYb+u5(q$5P-?19gl@ER5Y3fGU42gK:&+Q,^]\-7GD"nH-ahU?H\[78^EqXW
5AUR51V?F>[19-A&9`Fqr)m/Ut&>giX!nJf>;_ABgC-99oI.G"`;R%=Kf,*(51Le\AaU<k<S$*8V$s
#_F<7.T,2u`5.+SJUk[)L]?>/HhP1.q=-_P6@k7L<#JXQaqoOfl)X+?QFH`,a+LE\%^)3,ael13hqH
BFi^C.#&ZrVCRQOo^gc&_@I.te/d%Zn?cI@^/Pk.9eX/`2"WhLmWMa\211M=uUiZjLH_Ba\g11A\P^
rCmq<g?Tg.Pq;Y%e"l_N>*;agdq]I]=>UMBp_M$3e_8W&[DB/`:*PMjU-N2&W&aVP#&%!.*!a60]36
k%V!%.luh4;kFE]:YEl0<!dPCZM869,(NhVMG;h'DH=sY^QSF'+GK0:^DoJcniWIU.gZG']k+/Q1+N
WWqO++4;DLKBM:IfZl'F'oWYN5p!7&qG6>qVK"DU30S@>LXK)%]Z=1f$q5:dC,X`?s=G2#5;g2?+Z(
JLmpX,:7"TmVdm-(eYGO+oYP5oRVFa[13O[oU8+GPVWi7;]+SrclQ<h8us:82tQ2Cr()scOTu3oKg;
\_c+_!;^A<Q.3u0/Q2(m$7rV5`TB@5]#ZJMI(&^U$(B/sT*6%jK%=c[bmQZ#0*TI+"HLROQrDeG"_7
L1.;\MYHjG-%@0J2(G"U8!QBM,9&,TJ2j.@4d%1Xo;%^*ZV!s1i_a=Zl5CD'H3]s`WpM0KC,8\Fpak
KNKd=p_(@U.T#N?&hShX/q:jL!qt/X6htXeT_s^k%_s^hDY@$-$R#uW=p>1OEp[b`jj4cMJGEqlU([
pr2F[jP:p$4a5lR(^l7>A=%FcUIl?=.-IpHaW9+iLO^KFgJ+/B\#I3&mjN>&eQPPUTOA_D=M-;$^/-
<<U0s4E(;FJMkR56^U&#HBO%W9O/j.1,5'+a6JJuAoldHg:c$[2Ei_=_i23GD%1;Q1b:Wu/St.q2:k
htFtX<;<#Kq[1;9Zj-Tn<]=4CQk34AG!HXf;+DCdm]?'MEp)VMgjRA['WH)d=BK$t5"5]r-+<mk'^k
IK-C$j%"ENe^KRcd+NY@sE6ckBR&h7>Xm(1-W=ZN"h(+aEKsLT_aZHc_5MsFMgR#n_T^j-Dkgg<8,:
7E-&oMgO76t6u."H2@KHFOeSYd]#VE*\`_h8S[4c.bii^glt@g&+"cgJc#);n\#jOCG@5$p^UPM9:W
omYqnlY<pggpC!Pnh9-;7AV2#C?rq.FpHb#VMi=-ftOXE%>\cgs9F.uGNB&gG'_em"=b:!1Z&A"nlY
O\.m+8=6@&WuVU\gbUtWXWBIYMFP@CgY5BSj>5MoBcri^1c)'Nq<QG4DeF?jVc(&*XmF,-<$GM^s3d
pCB3C:>;3uLS/"r\=@nU-f/_TGo&DmE82UD<(d,P#/U<[FtHZW<`V)a$MUtePXY#;l(D.*=/0?]41M
QXbmAp;q1dj.5d&?:Q!^^+L^S4T-1:FW+qA1nHU9UOmoQX6'BD5\Hr3/BP-lGf`Z/i:d[-5S[#qB,'
!hI"%(B_/gIj9s>g6tYJ]'CrZ4PG,teZ\5eGiEP%MWbnl1hc`6/kpu+Y<UOWRm-kgG[kIFB>S6Ros*
W9+(]^<N0+_G4/m0cos0$:RASpiqM*Q3t'5W>c'c4P;/L:m!.?u$FUQqdH01!ZbdamtGi[I$=3h*Fc
1p)do3;D@(^jCh<rnZ*Xs)Db('0&Y9+HIaTYprKRAXk#t9*U^i/etN?J/=iGq;O1d'>BWQGNo+HH#;
EtWVO>45`pS9TJ394TH'4W.d*etfj?='GfuTBkI$"Z)$<0:,'-@#\M1-PWC.WMR$BD,.`3c`Ii#7pS
f'_4?4l[i[fY&V+7>-cCZ>>^p@(FJll^oDY[9hU1+o\E632$f0+`oV7@>^L7$PfH!_6a2CL^mjgf)T
t?d]\c8S<9ZJ]s$oJk3^m"0gihProT`#XkB5<55<J=-i>.MRlk].e&V>'>$Ta"_js'NNj?'`dgsCH.
O$!r&isERV8/d3OHg+Dm\]h_Mtn0*"u,;peLCjcG8Gb^g`"LKfO"5,4UmQN\I%:8L?Q0<m%M.=UF+Y
YFt&--UOSX9;?u0j7`<GIIm2'QF$k=Q7i#@b=fTJ\b)2Fnh4&qrCYVgDcJ@$X^^7+[?]4(Z%_"bq\"
If:]7:uQ@Mi(&OdJc;-K)9ShN?DJH\MgM,L2`J7T+,>IO%0mZr!O+(d+o-fngE.5YCGhU^,Z[&%316
	9n?fSA_D$SOu.X@$I!HKi'Qt.k&1]0,+1:SMHW!Cj`Urn^5'i`G;_=qgs%$"_Y2rk-b)QZj.ASL?H"
	"kI-)%>5^mlrh?RZr)WQ_D0>Sp2E`aDmVPo-_K_5<a"a1J3-2#Y'dVW;AOX)J_i*O5788=?e1lS)F1
	frha(WM\Z]]#3/T5CVd+#9cTjT#Z?3+2J:ngi[)&6^[@A4un.j.f&7/<WV)cW.tY:c7"jf(l?R2LMM
	SqOSW0=&%<?/=PCm.Qe6%?r+VQLss=p%Xkr2_7\k>+"rJPqKSsAck?YGY&O86E$,Ce)]p?3Mhst"VP
	#rl^o*5$i?+]pEf:Kbe&UaqY16^nZ7tXB+U&]Q9m^uY:JJ6lDGS%rDY,<+Rt&"AYQ3/X"Klh-)D`)^
	][JiX:Ud*:*qt,<@CcL;0'5p&?:OSAld!^6,!dpc`QX*Y1OPnC3>m0O0(SW/8ol\G)q,ELMpU&W2RU
	<eDW"EnWicmX4lAK6i]doM<(*ZH)dr*3L`I#<Cp5.*)Vt`_8[aO8KoO&YbCp]c?N(%]+g%X`o4B]NJ
	JGm[7fVF9ghCuba)TlLoE0PF@QH!3"'[@TN'.4]I4LplJSc5r:>gbg^A;><-?[_jJM5LB*GmL<$s((
	YR/)Y&1H06&8b6Bq9>ANDg"W0[!7m*4j.'HCKeE:8^G&P?tj\1BhuCM271"B=P4=gC_*[?1t9g_qc9
	U=MnHsl]Rj"G".47!_1Xpn6CqomX!4MEJ<O(@5^D<!'!W1.n0W57aB1UD6o8n&ga7aHC!@W77diheN
	:,IthupYZZ9'<t[M#T<iA)U0,/TV/+;S.HCp&UOXD`qZe.R2N?!m9JD*KXUpm(=*1#Y@bS9")S;d]A
	g<Nop'Y$Hrh)HX:&Y1W?_Jg54&5^XNq59[c*fj;8U%oq4eDGruC8DoTG+bNDlQ%MYOJVgBX:Z+6*H'
	k["BB5b#2`dkmk]Eht&mt]@(Vn&Un+J&Vd2K"'o_Z2aJ;GUb9Z.>3g3fD+&J;FXG.N1erN<p<=%qUB
	8JWUT:k4H!e93E@5S;p9hmfu2[gf`Tpt'`eg4%r2p3U/aP.8-&1Jk>0\t[GTP44]ZprX5@dR<_arkJ
	+!s.pNkPGk;i;)="#oUEtRLLgfF<M<k&@kO>Z4;4Y]#ZWDU6rZPu9>kl46q@G>G5-q.NK5G*+2<7SV
	[*j9h\p4-D@KZh\!5k@1"6XW1RQDiVbo6)F.ko3N0`af>ka88\hSfVdJn$0Wl_`=n/t@6=`P(R&E:d
	.]^.OfcFm:L_\JP)1bskSLdQtIe;oY!roqq;f2!K<"#EFZO!C5]*'.j@ZX"qJs1qbYST_>)6uXV*'+
	/u"*b,j#Rm<C4;TU8:6ma^OS-$@uFAZ8LWi']619rYQ=W,jlin[bCkG8os+4u&%=?GO>fkW$sef7E&
	gh@i(^&93.U5$\"8b[sqB9P]k#oj[>8uO4M9d18BGh:*\>9^=]eAfdmTq=QPBoZ3"[Biok5YNE^a`q
	V>YlPI(N2iUC'h'=2,F"tI.OeCjSPD7O0c?_b>(>Anmag8>*^0)jN[(arLt&PCO).,B%E&bhLH]ba;
	/RDL/SCjW>^oAUfD]UBCdoi!.m1!@9]BVYOKCq1A?c(0n[l]$;Uc:5Pq?[@q_WHo/GltP2Mg&J*V2.
	QaHD1sP=.tXA.U"/&eG@[)(;bH5Uss\%0^B56KJILp0]q6)+<j+C8VV-7/RVFa3dFfAm@"\&)&aAD<
	s(r"r`jiP;."nnOV<G0jM1Zj8l=^<Y-8Y`rn^TRCX@rM#"&Tj4b,&`0&H@`\3+:Lo-$+M'`]U%\eel
	qQ6EgOse&u/Y]!/]T[:e=d3Go&co`HJ,ZDnYk$*F4K&d"Au'WXB7":h(Jej?)jiO(6fH)Xo?cbl[]2
	Q$U_-D8rp&q6^`#cI?F99t's(Hdjg826O"#;.3[raln_jD;L`$l6d9/+rS'rP!+Z.O_Gr+M3T9:k[<
	E\>K17'A?<O.0dK7CY,FsLfHmRktc9OT8r`l,WH>B\%#9s5R$o\/7k"+t5Ul*<UkaCjg[bdQWpBJn"
	GT[MP.IH9dUn-Wn6c=Z$O(0B?\YX!aAY*fF7^s<r_24/YJCOnrZ3A3rcJsa@_C8jSFNKjt]>W8)-n[
	5>\qY#'NZiA_Unk/Ao/#2MImBP2/a7a/kA.sK^Ni=tor6H0cf`'b)n%ADo4!piR,H9(q[EhQ_S>A(t
	USe_$hqA$#eWP7nU+/P7i*?i9oRA@sm9/93Y'Kd"F-ntR$d`o[N,\mPYs_/1]>Dd[6]kbR_RmND]Xk
	/C1_H//)P3K*/l)V:G6WMQW=Mir*A]-$:Ca+:i6\-OQpSJ.omV>!5Nd(2gu+eG(!g'_F*;!aW+]icC
	'iJHi6&gNQ%,?Nq3UXqhH1ce<q`.JLuuKu?*kDpJ'M8[W?YmP_:KX#iEu!@h=8kt/H_o_&H@]6%.P/
	Q_qDN`lnceD4P%Yq6!sJfBJ9'L"64qIJ`kVhSmVAu=$b>A(A*n6Gpf$A4P0(/<4)6LP)+o=Lo1e[,+
	^Q05W8pB9UhoNCNB:k?nNlEV+othY<3nC-k/)!G/T_olUh]&k/D.hnN$-WV<!#-'2;CaE>UFbIXHUN
	p"rn)7[lu=W1Q9#pW!AaJ)&ZS7/r*-`"=Sb15p&&qbtYXBufW_J*YfQ]3IIFIM-cC%>.H8Y5-!r55*
	IH34'KER"*/SnG`L(I^kFcJ0)l6!!!!j78?7R6=>B
	ASCII85End
End


// popmenu in the PMF_PerformCalc_Panel changes the data and std dev wave name selection values
Function  pmfCalcs_popMenu_DataFolder(ctrlName,popNum,popStr) : PopupMenuControl
	String ctrlName
	Variable popNum
	String popStr

	svar DFNm=root:Temporary:DFNm
	if (stringmatch(popStr[0,4], "root:")) 	 // appears to be full path
		DFNm = popStr
	else
		popStr = "root:" + popStr
		DFNm = popStr			// hopefully give the full path
	endif

	setDataFolder $DFNm		// important

	//Also make just current expt name string
	string/g root:Temporary:ExptName = DFNm[5,20]

	//find min/max Dp for this project data and assign to dropdowns in integrated time series panel region.
	wavestats/q $DFNm + "SMPS:Raw:Dp"  // bbp v5.1 added /q flag
	make/o/n=1 minDp =  V_min*1e9
	make/o/n=1 maxDp =  V_max*1e9
	duplicate/o minDp, root:Temporary:minInt
	duplicate/o maxDp, root:Temporary:maxInt
	killwaves minDp, maxDp
	//	variable root:Temporary:minInt = V_min*1e9
	//	variable root:Temporary:maxInt =  V_max*1e9

End


// Returns a list with the current DF first, followed by the subfolders, sorted
Function/S gen_dataFolderList_wCurrDF(CurrentDF)
	string CurrentDF

	string existingDFName, existingDFList=""
	variable idex

	setDataFolder $CurrentDF

	// make list of DFs in root:, since I can't find a command to do this
	idex=0
	do
		existingDFName = GetIndexedObjName("root:", 4, idex)
		if (strlen(existingDFName) == 0) // no more DFs
			break
		endif
		//		if (!stringmatch("pmf_Plot_globals", existingDFName) )
		existingDFName += ":" // need trailing : for paths to work
		existingDFList = AddListItem(existingDFName, existingDFList) // add DF to list
		//		endif
		idex += 1
	while(1)

	// put the list in alphabetical order, 4 = case insensitive
	existingDFList = sortlist(existingDFList, ";", 4)

	// put the current DataFolder at the top of the list and return list
	existingDFList = AddListItem(currentDF,existingDFList)

	return(existingDFList)

End


Function ExpID2SMPS()

	variable avg1sTo10s = 0 //dd. 2018-06-22 added code to automatically average valve states (Expt. ID) to 10s (requires "GeneralMacros.ipf")
	if (avg1sTo10s) //if want preaveraging, switch avg1sTo10s to "=1" and uncomment lines in this if-loop
		// print "Pre-averaging valvestate/exptID to 10s"
		// duplicate/o root:MasterLogger:t_stop, ML_stop_1
		// duplicate/o root:MasterLogger:t_start, ML_start_1s
		// duplicate/o root:MasterLogger:ExpID, ExpID_1s
		// execute "AveragXSecs(\"ML_start_1s\",\"ExpID_1s\",\"ExpID\",\"my_std\",\"my_num\",10)"
		// duplicate/o start_10sec, ML_start; duplicate/o stop_10sec, ML_stop;
	else //don't do any preaveraging of valvestate/exptID
		duplicate/o root:MasterLogger:t_stop, ML_stop
		duplicate/o root:MasterLogger:t_start, ML_start
		duplicate/o root:MasterLogger:ExpID, ExpID
	endif

	make/o/d/n=(numpnts(ML_stop)) ML_mid = (ML_stop - ML_start)/2+ML_start
	wave Date_Time_Plot, ExpIDSMPS
	make/o/n=(numpnts(Date_Time_Plot)) ExpIDSMPS = -999
	make/o/n=(numpnts(Date_Time_Plot)) ExpIDSMPS_avg = -999
	variable thisDecile = 0
	variable iML
	for (iML=0; iML<numpnts(Date_Time_Plot); iML+=1)
		extract ExpID, theseExpIDs, (ML_mid[p]>Date_Time_Plot(iML) && ML_mid[p]<Date_Time_Plot(iML)+120) //120s scans. SMPS DATE_Time_Plot timestamp are start times.
		//print ScanLims_int(iML)
		if (numpnts(theseExpIDs)>0)
			wavestats/q/m=1 theseExpIDs
			ExpIDSMPS[iML] = round(V_avg)
			ExpIDSMPS_avg[iML] = V_avg
			//print theseExpIDs
		endif
		//  if((iML==500 || iML==1000) || (iML==30000 || iML==4000))  // give some idea of how far along the valve state assigment function
		//   print iML
		//  endif
		if (iML==thisDecile*round(numpnts(Date_Time_Plot)/10))  // give some idea of how far along the valve state assigment function
			print num2str(thisDecile*10) + "% through Experiment ID Interpolation to SMPS timestamp"
			thisDecile += 1
		endif
	endfor
	killwaves/z theseExpIDs, my_num, my_std, start_10sec, stop_10sec
End


//Used to export data to for making ICARTT files
//first change directory to Experiment where you want data exported from
Function ExportAmbient()

	SVAR ExpFolder = root:temporary:DFNm
	string ExpFold = ExpFolder+"SMPS:Plot"
	//SVAR ExpFolder = root:temporary:DFNm
	setdatafolder ExpFold

	wave dNdLogDp_Ambient
	duplicate/o dNdLogDp_2,  root:ExportData:dNdLogDp_Ambient
	duplicate/o dVdLogDp_2,  root:ExportData:dVdLogDp_Ambient
	duplicate/o TotconcN_2, root:ExportData:TotIntegratedNumber
	duplicate/o TotconcM_2, root:ExportData:TotIntegratedVolume
	duplicate/o TotTime_2, root:ExportData:TotTime_2
	duplicate/o Dp_Plot, root:ExportData:Dp

	setdatafolder root:ExportData
	//make start and end times.
	//all scans for BEACHON-RoMBAS were 240 seconds and AIM timestamp is scan start time
	//this info is contained in AIM files however not extracted with Mike C's loader
	wave TotTime_2, Dp
	make/o/d/n=(numpnts(TotTime_2)) Startscan = TotTime_2-3600 //Convert from MDT to MST
	make/o/d/n=(numpnts(Startscan)) Endscan = Startscan+240

	//build wave for dNdLogDp with start/end times as first columns for easier incorporation into ICARTT files
	duplicate/o Startscan, dNdLogDp_Ambient_wTimes
	wave dNdLogDp_Ambient_wTimes, dNdLogDp_Ambient
	concatenate {Endscan}, dNdLogDp_Ambient_wTimes
	concatenate {dNdLogDp_Ambient}, dNdLogDp_Ambient_wTimes
	//build wave for dVdLogDp with start/end times as first columns for easier incorporation into ICARTT files
	duplicate/o Startscan, dVdLogDp_Ambient_wTimes
	wave dVdLogDp_Ambient_wTimes, dVdLogDp_Ambient
	concatenate {Endscan}, dVdLogDp_Ambient_wTimes
	concatenate {dVdLogDp_Ambient}, dVdLogDp_Ambient_wTimes
	//build wave for Total Number and Total Volume with start/end times as first columns for easier incorporation into ICARTT files
	duplicate/o Startscan, TotIntegratedNumVol
	wave TotIntegratedNumber, TotIntegratedVolume, TotIntegratedNumVol
	concatenate {Endscan}, TotIntegratedNumVol
	concatenate {TotIntegratedNumber}, TotIntegratedNumVol
	concatenate {TotIntegratedVolume}, TotIntegratedNumVol

	make/o/n=(numpnts(Dp)) Dp_nm = Dp*1e9

	//build wave for dNdLogDp with Dp as first row for easier incorporation into ICARTT files
	duplicate/o Dp_nm, dNdLogDp_Ambient_wDp
	duplicate/o dNdLogDp_Ambient, temp_dNdLogDp_Ambient
	matrixtranspose temp_dNdLogDp_Ambient
	concatenate {temp_dNdLogDp_Ambient}, dNdLogDp_Ambient_wDp
	matrixtranspose dNdLogDp_Ambient_wDp
	//build wave for dVdLogDp with Dp as first row for easier incorporation into ICARTT files
	duplicate/o Dp_nm, dVdLogDp_Ambient_wDp
	duplicate/o dVdLogDp_Ambient, temp_dVdLogDp_Ambient
	matrixtranspose temp_dVdLogDp_Ambient
	concatenate {temp_dVdLogDp_Ambient}, dVdLogDp_Ambient_wDp
	matrixtranspose dVdLogDp_Ambient_wDp
	//for these files, need to have a start and end scan time with an empty spot at the top
	duplicate/o Startscan, Startscan_wNaN
	duplicate/o Endscan, Endscan_wNaN
	InsertPoints 0,1, Endscan_wNaN
	InsertPoints 0,1, Startscan_wNaN
	Startscan_wNaN[0] = Startscan[0]-1
	Endscan_wNaN[0] = Endscan[0]-1

	//build wave for dNdLogDp with Dp as first row and scan start/ends as first two columns for easier incorporation into ICARTT files
	//first make icartt timestamps with extra point at top to match height of Dp+dNdLogDm matix
	make/o/d/n=1 starttime = Startscan[0]
	killvariables/z startmonth startday startyear
	string/g startdate =  Secs2Date(starttime[0],-1)
	make/o/n=1 startday = str2num(startdate[0,1])
	make/o/n=1 startmonth = str2num(startdate[3,4])
	make/o/n=1 startyear = str2num(startdate[6,9])
	make/o/d/n=1 startsecs = date2secs(startyear, startmonth, startday)
	duplicate/o Startscan_wNaN, Startscan_icartt
	duplicate/o Endscan_wNaN, Endscan_icartt
	Startscan_icartt = Startscan_wNaN - startsecs
	Endscan_icartt = Endscan_wNaN - startsecs
	//now make combined dNdLogDm, Dm, Scantimes matrix
	duplicate/o Startscan_icartt, dNdLogDp_Ambient_wTimes_wDp
	concatenate {Endscan_icartt}, dNdLogDp_Ambient_wTimes_wDp
	concatenate {dNdLogDp_Ambient_wDp}, dNdLogDp_Ambient_wTimes_wDp
	//now make combined dVdLogDm, Dm, Scantimes matrix
	duplicate/o Startscan_icartt, dVdLogDp_Ambient_wTimes_wDp
	concatenate {Endscan_icartt}, dVdLogDp_Ambient_wTimes_wDp
	concatenate {dVdLogDp_Ambient_wDp}, dVdLogDp_Ambient_wTimes_wDp

	//export text files (when prompted make file end date current) - these ones contain the start and end time as the first columns
	//Save/J/M="\r\n"/W dNdLogDp_Ambient_wTimes as "dNdLogDp_Ambient_thru1107XX.txt"
	//Save/J/M="\r\n"/W dVdLogDp_Ambient_wTimes as "dVdLogDp_Ambient_thru1107XX.txt"
	//Save/J/M="\r\n"/W TotIntegratedNumVol as "TotIntegratedNumVol_thru1107XX.txt"

	//export text files (when prompted make file end date current) - these ones contain the start and end time as the first columns
	//Save/J/M="\r\n"/W dNdLogDp_Ambient_wTimes as "dNdLogDp_Ambient_thru1107XX.txt"
	//Save/J/M="\r\n"/W dVdLogDp_Ambient_wTimes as "dVdLogDp_Ambient_thru1107XX.txt"
	//Save/J/M="\r\n"/W TotIntegratedNumVol as "TotIntegratedNumVol_thru1107XX.txt"

	//export text files (when prompted make file end date current) - these ones contain the Dp in the top row but not the scan times
	//Save/J/M="\r\n"/W dNdLogDp_Ambient_wDp as "dNdLogDp_Ambient_thru1108XX.txt"
	//Save/J/M="\r\n"/W dVdLogDp_Ambient_wDp as "dVdLogDp_Ambient_thru1108XX.txt"

	//export text files (when prompted make file end date current) - these ones contain the Dp in the top row but not the scan times
	//first convert nans to -9999s
	dNdLogDp_Ambient_wTimes_wDp = numtype(dNdLogDp_Ambient_wTimes_wDp)==2 ? -9999 : dNdLogDp_Ambient_wTimes_wDp
	dVdLogDp_Ambient_wTimes_wDp = numtype(dVdLogDp_Ambient_wTimes_wDp)==2 ? -9999 : dVdLogDp_Ambient_wTimes_wDp
	Save/J/M="\r\n"/W dNdLogDp_Ambient_wTimes_wDp as "dNdLogDp_Ambient_thru1108XX.txt"
	Save/J/M="\r\n"/W dVdLogDp_Ambient_wTimes_wDp as "dVdLogDp_Ambient_thru1108XX.txt"

	//Save/J/M="\r\n"/W TotIntegratedNumVol as "TotIntegratedNumVol_thru1108XX.txt"
	Save/J/M="\r\n"/W Startscan, Endscan, TotIntegratedNumber, TotIntegratedVolume  as "TotIntegratedNumVol_thru1108XX.txt"

	killwaves/z TotTime_2 temp_dNdLogDp_Ambient, temp_dVdLogDp_Ambient
	setdatafolder ExpFolder
End


Function BadT(startbad_str, endbad_str)
	string startbad_str
	string endbad_str
	make/o/d/n=1 month =  str2num(startbad_str[0,1])
	make/o/d/n=1 day =  str2num(startbad_str[3,4])
	make/o/d/n=1 year =  str2num(startbad_str[6,9])
	make/o/d/n=1 hour =  str2num(startbad_str[11,12])
	make/o/d/n=1 minute =  str2num(startbad_str[14,15])
	make/o/d/n=1 second =  str2num(startbad_str[17,18])
	make/o/d/n=1 startbad = date2secs(year, month, day) + hour*3600+minute*60+second
	make/o/d/n=1 month =  str2num(endbad_str[0,1]);
	make/o/d/n=1 day =  str2num(endbad_str[3,4]);
	make/o/d/n=1 year =  str2num(endbad_str[6,9])
	make/o/d/n=1 hour =  str2num(endbad_str[11,12])
	make/o/d/n=1 minute =  str2num(endbad_str[14,15])
	make/o/d/n=1 second =  str2num(endbad_str[17,18])
	make/o/d/n=1 endbad = date2secs(year, month, day) + hour*3600+minute*60+second
	wave BadStart_SMPS, BadEnd_SMPS
	concatenate/NP {startbad}, BadStart_SMPS
	concatenate/NP {endbad}, BadEnd_SMPS
	killwaves month, day, year, hour, minute, second, endbad, startbad
End


Function CutBadDat()
	make/o/d/n=0 BadStart_SMPS
	make/o/d/n=0 BadEnd_SMPS
	///***These are are all start scan times for SMPS; for "Big Filters" cut only Ambient and TD
	///these should be Daylight times (convert to Standard Mountain time later for export for icartt files)
	//Also be careful b/c it appear that when looking at start scan time in AIM software everything if moved back 1 hour! (as if converted to standard time but not when exporting?!?!)

	////  A few examples cutting periods of bad data, filters, etc. "manually" for BEACHON-RoMBAS.
	/////  Just make a list and document reason for cut
	//switching from LiqN2 bypass to gas pump
	//BadT("07/26/2011 15:30:00", "07/26/2011 17:02:00")
	//BadT("07/26/2011 16:50:00", "07/26/2011 17:02:00")
End


//this function will "manually" assign ExpID states to periods where Masterlogger data missing or incorrect
Function ReAssignExpIDStates()
	wave ExpIDSMPS, tempDate_Time_Plot

	//reassign ExpID states to ambient
	make/o/d/n=0 MakeAmbient_2_start
	make/o/d/n=0 MakeAmbient_2_end
	ToChangeExpIDState("08/24/2011 15:35:00", "08/24/2011 16:07:00")

	//reassign ExpID states to ambient
	//first create temp variable if not made earlier
	if (waveexists(tempDate_Time_Plot))
		//do nothing
	else
		duplicate/o Date_Time_Plot, tempDate_Time_Plot
	endif

	variable idexx
	for (idexx=0; idexx<numpnts(MakeAmbient_2_start); idexx+=1)
		ExpIDSMPS = (tempDate_Time_Plot[p]>=MakeAmbient_2_start[idexx] && tempDate_Time_Plot[p]<=MakeAmbient_2_end[idexx]) ? 2 : ExpIDSMPS[p]
	endfor

	//reassign ExpID states to NO3PAM that were recorded as ExpID state = 0 (normally TD) - i.e. TD was offline during this period
	make/o/d/n=0 MakeAmbient_2_start
	make/o/d/n=0 MakeAmbient_2_end
	ToChangeExpIDState("08/02/2011 14:00:00", "08/09/2011 20:00:00")

	//reassign ExpID states to NO3
	//first create temp variable if not made earlier
	if (waveexists(tempDate_Time_Plot))
		//do nothing
	else
		duplicate/o Date_Time_Plot, tempDate_Time_Plot
	endif

	for (idexx=0; idexx<numpnts(MakeAmbient_2_start); idexx+=1)
		ExpIDSMPS = (tempDate_Time_Plot[p]>=MakeAmbient_2_start[idexx] && tempDate_Time_Plot[p]<=MakeAmbient_2_end[idexx]) && ExpIDSMPS[p] == 0 ? 3 : ExpIDSMPS[p]
	endfor

	killwaves/z tempDate_Time_Plot, temp_Date_Time_Plot
End

//reuse function that cuts bad data for reassigning ExpID state to ambient
Function ToChangeExpIDState(startbad_str, endbad_str)
	string startbad_str
	string endbad_str
	make/o/d/n=1 month =  str2num(startbad_str[0,1])
	make/o/d/n=1 day =  str2num(startbad_str[3,4])
	make/o/d/n=1 year =  str2num(startbad_str[6,9])
	make/o/d/n=1 hour =  str2num(startbad_str[11,12])
	make/o/d/n=1 minute =  str2num(startbad_str[14,15])
	make/o/d/n=1 second =  str2num(startbad_str[17,18])
	make/o/d/n=1 startbad = date2secs(year, month, day) + hour*3600+minute*60+second
	make/o/d/n=1 month =  str2num(endbad_str[0,1]);
	make/o/d/n=1 day =  str2num(endbad_str[3,4]);
	make/o/d/n=1 year =  str2num(endbad_str[6,9])
	make/o/d/n=1 hour =  str2num(endbad_str[11,12])
	make/o/d/n=1 minute =  str2num(endbad_str[14,15])
	make/o/d/n=1 second =  str2num(endbad_str[17,18])
	make/o/d/n=1 endbad = date2secs(year, month, day) + hour*3600+minute*60+second
	wave BadStart_SMPS, BadEnd_SMPS
	concatenate/NP {startbad}, MakeAmbient_2_start
	concatenate/NP {endbad}, MakeAmbient_2_end
	killwaves month, day, year, hour, minute, second, endbad, startbad
End


//create mask of ambient scans that directly follow PAM b/c often there is carryover of small particles
Function postPAMmask()
	wave ExpIDSMPS
	duplicate/o ExpIDSMPS, postPAMambient
	postPAMambient = ExpIDSMPS[p] - ExpIDSMPS[p-1]
	postPAMambient = postPAMambient==1 ? 1 : 0
	extract postPAMambient, postPAMambient_Paired, ExpIDSMPS==2
End


//additional filtering/QC needed
//none so far

duplicate/o TotconcM_2, TotconcM_2_postPAM
duplicate/o TotconcM_2, TotconcM_2_noPAM
TotconcM_2_noPAM = postPAMambient_Paired==0 ? TotconcM_2_noPAM[p] : nan
TotconcM_2_postPAM = postPAMambient_Paired==1 ? TotconcM_2_postPAM[p] : nan

duplicate/o TotconcN_2, TotconcN_2_postPAM
duplicate/o TotconcN_2, TotconcN_2_noPAM
TotconcN_2_noPAM = postPAMambient_Paired==0 ? TotconcN_2_noPAM[p] : nan
TotconcN_2_postPAM = postPAMambient_Paired==1 ? TotconcN_2_postPAM[p] : nan



function checkDF()

	if(datafolderexists("root:Y"))

		print "data folder does exist"

	else

		print "data folder does not exist"

	endif


END



//Coding added directly to relevant functions above.
//Function CheckSameDpRange()
//       wave Dp
//       make/o/n=(dimsize(Dp,1)) thisDp = Dp[0][p]
//       wavestats/q thisDp
//       variable file1minDp = V_min
//       variable file1maxDp = V_max
//       make/o/n=(dimsize(Dp,0)) samemin = nan
//       make/o/n=(dimsize(Dp,0)) samemax = nan
//       variable idex
//       for (idex=0; idex<(dimsize(Dp,0)); idex+=1)
//       make/o/n=(dimsize(Dp,1)) thisDp = Dp[idex][p]
//       wavestats/q thisDp
//       samemin[idex] = V_min==file1minDp
//       samemax[idex] = V_max==file1maxDp
//       endfor
//       wavestats/q samemin
////       make/o/n=1 AllsameMinDp = V_avg==1
//       variable AllsameMinDp = V_avg==1
//       wavestats/q samemax
////       make/o/n=1 AllsameMaxDp = V_avg==1
//       variable AllsameMaxDp = V_avg==1
//       killwaves/z file1minDp file1maxDp samemin samemax thisDp thismaxDp thisminDp, samemins, samemaxs
//End



////////Convert diameter midpoints to diameter boundaries; Assumes lognormal spacing///////////////////////////////
//(adopted/renamed from DD_Gentools.ipf)
Function SMPS_DiamMids2Lims(Dx) //output is Dx_Lim, Dx_mid
	wave Dx
	duplicate/o Dx, Dx_mid
	make/o/n=(numpnts(Dx)) LogDx_mid = log(Dx_mid)
	make/o/n=(numpnts(Dx)-1) dLogDx_mid = LogDx_mid[p+1]-LogDx_mid[p]
	Redimension/N=(numpnts(dLogDx_mid)+1) dLogDx_mid
	InsertPoints 0,1, dLogDx_mid
	dLogDx_mid[0] = dLogDx_mid[1]
	dLogDx_mid[numpnts(dLogDx_mid)-1] = dLogDx_mid[numpnts(dLogDx_mid)-2]
	make/o/n=(numpnts(Dx)) LogDx_lim = LogDx_mid[p]-dLogDx_mid[p]/2
	Redimension/N=(numpnts(LogDx_lim)+1) LogDx_lim
	LogDx_lim[numpnts(LogDx_lim)-1] = LogDx_mid[numpnts(LogDx_mid)-1]+dLogDx_mid[numpnts(dLogDx_mid)-1]/2
	make/o/n=(numpnts(Dx)+1) Dx_lim = 10^LogDx_lim
	killwaves LogDx_mid, dLogDx_mid, LogDx_lim
End

////////////////////SMPS_dS/////////////////////////////////////////////////////////////////////////////////
// For number to surface area conversion	  (from DS_dM, adopted/renamed from DD_Gentools.ipf)
Function SMPS_dS(dNdlogDp,dpM,row,col)
	Wave dNdlogDp
	Wave dpM			// corresponding MIDPOINT diameter wave, SI units (ie metres, not nm, or um)
	Variable row,col		// p, q

	Variable Nlog = dNdlogDp[row][col]
	return 1e12*Nlog*4*pi*(DpM[col]/2)^2
End


////////////////SMPS_FS_corr_calc/////////////////////////////////////////////////////////////////////////////////////
// calculates Fuchs-Sutugin cond. sink coefficient as a function of size
//adopted renamed from from DD_Gentools.ipf)
function SMPS_FS_corr_calc(molar_mass,temp_K,Diff,alpha,GF, dp_midp_metres)
	variable molar_mass,temp_K,Diff,alpha,GF,dp_midp_metres

	variable Kn

	variable k_b = 1.3806e-23

	variable mfp = 3 * sqrt((pi*molar_mass/6.022e26)/(8*k_b*temp_K)) * (Diff /1e4)

	Kn = mfp / (GF*dp_midp_metres/2)

	return (Kn+1) / (0.377*Kn + 1 + (4/3)*alpha^-1*Kn^2 + (4/3)*alpha^-1*Kn)

end


////////////////////////JG_dNdlogDp2Total/////////////////////////////////////////////////////////////////////////////
// originally adopted from JG_dNdlogDp2Total() bbp 2/26/15 - takes dNdLogDp (and various variables/waves) to calculate integrated Condensational Sink
// this version adopted/renamed from DD_GenTools.ipf
Function SMPS_dCSdlogDp2Total(MW,Temperature,diff,alpha,Source,Dest,dp_midp_metres [,dp_start, dp_end, GF])

	wave MW //condensing gas molecular weight (g/mol)
	wave Temperature //temperature (K)
	wave diff,alpha //diff: gass diffusion coefficient (cm2/s), alpha: sticking coefficient (unitless, 0-1)
	Wave Source	// a dn/dlogdp 2-D matrix of rows=time, cols=dp
	Wave Dest		// destination 1-D wave
	Wave dp_midp_metres	// corresponding dp wave for source (meters)
	variable dp_start	// optional dp at which to start summing
	variable dp_end	// and end
	wave GF //aerosol growth factor (unitless: 1 for not growth)

	if(ParamIsDefault(GF))
		make/o/n=(numpnts(Dest)) root:GF_1/wave=GF
		GF = 1
	endif

	variable n, sp, ep, nanout

	variable vGF, vTemp, vDiff, vMW, vAlpha
	make/o/n=(numpnts(dp_midp_metres)) FS_corr
	duplicate/o Source, SourceOrig
	Source = numtype(Source)==0 ? Source : 0 //DD 1/12/11 (convert nans to zeros so can be properly summed when nans present)

	make/n=(numpnts(dp_midp_metres))/free logdp = log(dp_midp_metres)
	make/n=(dimsize(source,1))/free column
	redimension/n=(dimsize(source,0)) dest

	for (n=0; n<dimsize(source,0); n+=1)
		column = source[n][p]
		// To deal with possible NaN's at beginning and end of matrix from interpolating the dp
		nanout = 1
		sp=-1
		do
			sp+=1
			if (sp==numpnts(column))
				nanout = nan
				break
			endif
		while (numtype(column[sp])!=0)
		ep=numpnts(column)
		do
			ep-=1
			if (ep==0)
				nanout = nan
				break
			endif
		while (numtype(column[ep])!=0)

		sp = paramisdefault(dp_start) ? dp_midp_metres[sp] : max(dp_midp_metres[sp],dp_start)
		ep = paramisdefault(dp_start) ? dp_midp_metres[ep] : min(dp_midp_metres[ep],dp_end)


		// finally, assign values (in some cases, if input as single value, extend here to full time series lenght as repeated value)
		vGF = GF[n]
		vTemp = Temperature[n]
		if (numpnts(Alpha) < numpnts(column))
			vAlpha = Alpha[0]
		else
			vAlpha = Alpha[n]
		endif
		if (numpnts(Diff)<numpnts(column))
			vDiff = Diff[0]
		else
			vDiff = Diff[n]
		endif
		if (numpnts(MW)<numpnts(column))
			vMW = MW[0]
		else
			vMW = MW[n]
		endif
		FS_Corr = SMPS_FS_Corr_calc(vMW,vTemp,vdiff,valpha,vGF,dp_midp_metres)
		column *= 4*pi*vDiff*FS_corr * (vGF*dp_midp_metres*100/2) // This now calculates the actual rate coefficient of the condensational sink in units (s-1)
		dest[n] = nanout*areaXY(logdp,column,log(sp),log(ep))
	endfor
	duplicate/o SourceOrig, Source //dd change back to original waves with nans rather than zeros
	killwaves/z SourceOrig, FS_corr, root:Kn
End


//adopted/renamed from DS_Gentools.ipf
Function/S SMPS_Colon(Str [,none])	// MJC 6/16/2005 Puts a colon on the end of a string if it doesn't have one.
	// or does the reverse if you set none=1
	String Str
	Variable none

	if (ParamIsDefault(none))		// Put colon on end
		if (!stringmatch(str, "*:"))
			str += ":"
		endif
	else						// Take colon off end
		if (stringmatch(str, "*:"))
			String endstr=""
			Variable n
			for (n=0; n<ItemsInList(str, ":"); n+=1)
				endstr  += StringFromList(n, str, ":")
				if (n<(ItemsInList(str, ":") - 1))
					endstr += ":"
				endif
			endfor
			str = endstr
		endif
	endif

	return str

End

///adopted/renamed from DS_Gentools.ipf
//This function was copied in here from Tools_20100221.ipf because it was altered when copying to JG_GenTools_v1.ipf that made it incompatible with the JGMasterParseFunctions. BBP
Function/S SMPS_LoadDir(ext, SMPS_LoadFunc, ListofWaves, Format, StartLooking, StopLooking, offset, delta [, ListOfWavesDims, RowFactor, FileWave])
	//MJC 7/14/2005 - Template for loading a whole directory of files with extension <ext>
	String ext							// use "????" for all files in directory
	Funcref SMPS_ErrorFunc SMPS_LoadFunc			//name of function that loads each individual file
	// (refnum, offset, delta, WaveList)
	String ListOfWaves					// Waves to CREATE and pass to loading function, e.g "root:Riverside:Counts;root:Riverside:Dp"
	//	note semicolon separated, no spaces
	String Format						// file format: e.g. "CCN data YYMMDDhhmmss.dat" to show where date and time values are in filename
	Variable StartLooking, StopLooking	// in igor secs, use 0 to do entire directory
	Variable offset, delta					// offset and delta if you are making scaled waves in parsing function
	String ListOfWavesDims				// column numbers for waves to create (if not 1), eg "1;3;1;2" - must be same
	// 	number of list items as in ListOfWaves
	Variable RowFactor					// Option to create waves as more than one row. If set, waves will be created
	// 	with NumFiles*RowFactor rows
	Wave/t FileWave					// TEXT wave which will contain filenames corresponding to data at each point
	FUNCREF SMPS_ErrorFunc thisFunction = SMPS_LoadFunc


	if (!ParamIsDefault(FileWave) && !WaveExists(FileWave))
		abort "Cannot find FileWave in LoadDir function"
	elseif(!ParamIsDefault(FileWave))
		ReDimension/n=0 FileWave
	endif

	NewPath/o LoadDirPath
	If (V_Flag)
		abort
	endif

	String OrgFileList = IndexedFile(LoadDirPath, -1, ext)		// Gets list of files matching extension. In name order.
	String DestFileList = "", CurrentFile
	Variable n, NumFiles = ItemsInList(OrgFileList), refnum

	if (ParamIsDefault(ListOfWavesDims))
		SMPS_CreateWaves(ListOfWaves)
	elseif (ParamIsDefault(RowFactor))
		SMPS_CreateWaves(ListOfWaves, Dims=ListOfWavesDims)
	else
		SMPS_CreateWaves(ListOfWaves, Dims=ListOfWavesDims, Rows=RowFactor*NumFiles)
	endif

	SMPS_CreateFolder("root:Temporary")
	Make/d/o/n=(NumFiles) root:temporary:LoadDir_Time
	Make/t/o/n=(NumFiles) root:temporary:LoadDir_File
	Wave LoadDir_Time = root:temporary:LoadDir_Time
	Wave/t LoadDir_File = root:temporary:LoadDir_File
	String/G root:temporary:LoadDir_Status = "Loading file # of #"
	SVAR LoadDir_Status = root:temporary:LoadDir_Status

	if (WinType("LoadDirPanel"))
		KillWindow LoadDirPanel
	endif
	NewPanel /W=(39,70,234,121) /N=LoadDirPanel
	TitleBox LoadDirTitleBox,pos={19,15},size={128,21},fSize=10,fStyle=1
	TitleBox LoadDirTitleBox,variable= root:Temporary:LoadDir_Status,anchor= MC

	// Sort file list into date+time order

	for (n=0; n<NumFiles; n+=1)
		LoadDir_Time[n] = SMPS_Format2Secs(Format, StringFromList(n,OrgFileList))
		LoadDir_File[n] = StringFromList(n,OrgFileList)
	endfor
	Sort LoadDir_Time, LoadDir_Time, LoadDir_File
	for (n=0; n<NumFiles; n+=1)
		DestFileList += LoadDir_File[n] + ";"
	endfor

	StartLooking = StartLooking == 0 ? LoadDir_Time[0]: StartLooking
	StopLooking = StopLooking == 0 ? LoadDir_Time[numpnts(LoadDir_Time)-1] : StopLooking

	Variable sp = min(0, binarysearch(LoadDir_Time, StartLooking))
	Variable ep = binarysearch(LoadDir_Time, StopLooking)
	ep = ep < 0 ? numpnts(LoadDir_Time) - 1 : ep

	// Call parsing function for each file, in date/time order

	for (n=sp; n<=ep;n+=1)
		CurrentFile = LoadDir_File[n]
		Open/R/P=LoadDirPath /Z refnum as CurrentFile
		LoadDir_Status = "Loading file "+num2str(n+1)+" of "+num2str(NumFiles)+"."
		doupdate
		if (refnum<=0)
			abort "Error in LoadDir - could not find filename!"
		else
			String ListInclFileName = ListOfWaves+";"+S_filename
			thisFunction(refnum, offset, delta, ListInclFileName)
			if (!ParamIsDefault(FileWave))
				SMPS_InsertTextInLastRow(FileWave,S_filename)
			endif
			Close refnum
		endif
	endfor

	Killwaves /z LoadDir_Time, LoadDir_File
	KillStrings LoadDir_Status
	KillWindow LoadDirPanel

End


// DTS A prototype like function for use in the LoadDir function.
//Adopted/renamed from DS_Gentools.ipf
Function SMPS_ErrorFunc(refnum, offset, delta, ListOfWaves)
	Variable refnum, offset, delta
	String ListOfWaves

	abort "Error in function name"		// this means that the function it was supposed to call was mistyped

End


// DTS general use function for creating global waves  initialized to nans for future use
//Adopted/renamed from DS_Gentools.ipf
Function SMPS_CreateWaves(ListOfWaves [,printresults, dims, rows])
	String ListofWaves			// Waves to CREATE and pass to loading function, e.g "root:Riverside:Counts;root:Riverside:Dp"
	//	note semicolon separated, no spaces
	Variable printresults			// set to 1 to print waves names in command box
	String dims					// if not single column waves... then make a list saying how many columns eg "1;2;6",
	//     needs to be same num list items as ListOfWaves!!!
	Variable rows				// Number of rows in all the waves

	Variable CreateRows, n, k, NumWaves, NumDirs, cols
	String CurrentWave, CurrentDir

	if (ParamIsDefault(rows))
		CreateRows = 0
	else
		CreateRows = Rows
	endif

	NumWaves = ItemsInList(ListOfWaves)

	for (n=0; n<NumWaves;n+=1)
		CurrentWave = StringFromList(n,ListOfWaves)
		NumDirs = ItemsInList(CurrentWave, ":") - 1
		k=0
		CurrentDir = ""
		do
			CurrentDir += StringFromList(k, CurrentWave, ":") + ":"
			k+=1
		while (k<NumDirs)
		SMPS_CreateFolder(CurrentDir)
		if (ParamIsDefault(dims))
			Make/d/o/n=1 $CurrentWave
		else
			if (ItemsInList(ListOfWaves)==ItemsInList(Dims))
				if (stringmatch(StringFromList(n,Dims), "t"))
					Make/o/t/n=(CreateRows) $CurrentWave = ""
				else
					cols = str2num(StringFromList(n,Dims))
					if (cols==1)
						Make/o/d/n=(CreateRows) $CurrentWave = NaN
					else
						Make/o/d/n=(CreateRows,cols) $CurrentWave = NaN
					endif
				endif
			else
				abort "Error in CreateWaves function - ListOfWaves and Dims are not same length!"
			endif
		endif
		if (!ParamIsDefault(printresults))
			Print "Created wave = "+ GetWavesDataFolder($CurrentWave, 2)
		endif
	endfor

	return NumWaves

End


// Checks if a folder exists and creates if not
// Returns number of folders created
// DTS general purpose folder creation for arbitrary depths
//Adopted/renamed from DS_Gentools.ipf
Function SMPS_CreateFolder(DestPath [,wv])
	String DestPath	// Full path
	Variable wv		// if DestPath includes a wave name, set to 1 to avoid creating this as a folder

	String savedDF = GetDataFolder(1)
	Variable i, n, NumFolders
	String Path = ""

	if (!ParamIsDefault(wv))
		for (n=0; n<ItemsInList(DestPath,":")-1;n+=1)
			Path+=StringFromList(n,DestPath,":")+":"
		endfor
		DestPath = Path
	endif

	if (!DataFolderExists(DestPath))
		NumFolders = ItemsInList(DestPath, ":")
		Path = ""
		for (i=0; i<NumFolders; i+=1)
			path += StringFromList(i, DestPath, ":")+":"
			if (!DataFolderExists(Path))
				break
			else
				SetDataFolder $Path
			endif
		endfor
		for (n=i; n<NumFolders; n+=1)
			NewDataFolder /S $StringFromList(n, DestPath, ":")
		endfor

		SetDataFolder savedDF
		return NumFolders-i
	else
		return 0
	endif

End


//Adopted/renamed from DS_Gentools.ipf
Function SMPS_InsertTextInLastRow(Dest, Text)
	Wave/t Dest
	String Text

	if (dimsize(Dest,1))
		DoAlert 0, "2+D wave passed to InsertInLastRow- cannot act on this"
		return 0
	endif

	Variable Rows = numpnts(Dest)
	ReDimension/n=(Rows+1) Dest
	Dest[Rows] = text

End


//Adopted/renamed from DS_Gentools.ipf
Function SMPS_dM(dNdlogDp,dpN,dpM,row,col,rho)
	Wave dNdlogDp		// Must be a dN/dlogDp wave, as log->linear conversion is applied in this function
	Wave dpN			// corresponding BOUNDARY diameter wave
	Wave dpM			// corresponding MIDPOINT diameter wave, SI units (ie metres, not nm, or um)
	Variable row,col		// p, q
	Variable rho			// density g cm-3

	Variable Nlog = dNdlogDp[row][col]
	Variable Nlinear = Nlog * log((DpN[col+1]) / (DpN[col]))
	Variable Mlinear = rho*1e18*Nlinear*4*pi/3*(DpM[col]/2)^3
	//	return Mlinear / log((DpM[col+1]) / (DpM[col]))
	return Mlinear / log((DpN[col+1]) / (DpN[col]))  // bbp 16 feb 2015

End


////////Integrated dXdLogDx to Total X////////////////////////////////////////////////////////////////////////////////////////////////////////////
////Adopted/renamed from DS_Gentools.ipf (originally from "DS_SMPS.ipf")
Function SMPS_dNdlogDp2Total(Source,Dest,dp_midp_metres [,dp_start, dp_end])	// a new function to do this explicitly
	Wave Source	// a dn/dlogdp 2-D matrix of rows=time, cols=dp
	Wave Dest		// destination 1-D wave
	Wave dp_midp_metres	// corresponding dp wave for source, must be in SI units
	variable dp_start	// optional dp at which to start summing
	variable dp_end	// and end

	variable n, sp, ep, nanout


	duplicate/o Source, SourceOrig
	Source = numtype(Source)==0 ? Source : 0 //DD 1/12/11 (convert nans to zeros so can be properly summed when nans present)

	make/n=(numpnts(dp_midp_metres))/free logdp = log(dp_midp_metres)
	make/n=(dimsize(source,1))/free column
	redimension/n=(dimsize(source,0)) dest
	for (n=0; n<dimsize(source,0); n+=1)
		column = source[n][p]
		// To deal with possible NaN's at beginning and end of matrix from interpolating the dp
		nanout = 1
		sp=-1
		do
			sp+=1
			if (sp==numpnts(column))
				nanout = nan
				break
			endif
		while (numtype(column[sp])!=0)
		ep=numpnts(column)
		do
			ep-=1
			if (ep==0)
				nanout = nan
				break
			endif
		while (numtype(column[ep])!=0)

		sp = paramisdefault(dp_start) ? dp_midp_metres[sp] : max(dp_midp_metres[sp],dp_start)
		ep = paramisdefault(dp_start) ? dp_midp_metres[ep] : min(dp_midp_metres[ep],dp_end)


		// finally, assign values
		dest[n] = nanout*areaXY(logdp,column,log(sp),log(ep))
	endfor
	duplicate/o SourceOrig, Source //dd change back to original waves with nans rather than zeros
	killwaves/z SourceOrig
End


//Adopted/renamed from DS_Gentools.ipf
// originally given by Doug Day for SMPS stuff
Function SMPS_dN2dV(dNdLogDx, Dp, [,Dva]) //Always need Dp for geometric size calculation, Dva is options and needed only if input is dVdLogDva
	wave dNdLogDx
	wave Dp
	wave Dva

	//if Dp is wave, convert to matrix
	if (dimsize(dNdLogDx,0) != dimsize(Dp,0))
		duplicate/o dNdLogDx, Dp_matrix_nm
		Dp_matrix_nm = Dp[q]
	else //already matrix so just rename
		duplicate/o Dp, Dp_matrix_nm
	endif

	//compute dNdLogDx
	duplicate/o dNdLogDx, dVdLogDx
	duplicate/o Dp_matrix_nm, SingleParticleVolume_matrix_nm3
	SingleParticleVolume_matrix_nm3 = 4/3*pi*((Dp_matrix_nm)/2)^3
	dVdLogDx = dNdLogDx*SingleParticleVolume_matrix_nm3*(10^-9)//10^-9 converts nm3=>um3

	//compute integrated number concentrations
	make/o/n=(dimsize(dNdLogDx,0)) TotV = nan
	if(paramisdefault(Dva))
		make/o/n=(dimsize(Dp_matrix_nm,1)) Dp_1D = Dp_matrix_nm[0][p]
		SMPS_dNdlogDp2Total(dVdLogDx, TotV, Dp_1D, dp_start = Dp_1D[0], dp_end = Dp_1D[numpnts(Dp_1D)])
	else
		SMPS_dNdlogDp2Total(dVdLogDx, TotV, Dva, dp_start = Dva[0], dp_end = Dva[numpnts(Dva)])
	endif

	killwaves/z SingleParticleVolume_matrix_nm3, Dp_matrix_nm, Dp_1D
End

// Converts every occurence of OrgNum to ReplNum in either: Wave "Source" or All waves in Folder (full path from root) that come up using or WaveList(matchStr, ";", OptionsStr)
// Function will operate on Source AND Folder list if both specified
//Adopted/renamed from DS_Gentools.ipf
Function SMPS_NaN2Num(OrgNum, ReplNum [, Source, Folder, MatchStr, OptionsStr, PrintOutput])	// MJC 4/27/05
	Variable OrgNum
	Variable ReplNum
	Wave Source
	String Folder
	String MatchStr		// defaults to "*" if not specified
	String OptionsStr		// defaults to "" if not specified
	Variable PrintOutput	// Set to print wavelist altered in command box

	Variable n
	String Waves2Convert = ""
	String savedDF = GetDataFolder(1)

	if (!ParamIsDefault(Source))
		if (WaveExists(Source))
			Waves2Convert = GetWavesDataFolder(Source,2) + ";"
		endif
	endif
	if (!ParamIsDefault(Folder))
		if (ParamIsDefault(MatchStr))
			MatchStr = "*"
		endif
		if (ParamIsDefault(OptionsStr))
			OptionsStr=""
		endif
		SetDataFolder $Folder
		Waves2Convert += WaveList(MatchStr, ";", OptionsStr)
	endif

	for (n=0; n<ItemsInList(Waves2Convert);n+=1)
		Wave Convert = $StringFromList(n, Waves2Convert)
		if (numtype(OrgNum)==2)
			Convert = numtype(convert[p][q][r])==2 ? ReplNum : convert[p][q][r]
		else
			Convert = convert[p][q][r] == OrgNum ? ReplNum : convert[p][q][r]
		endif
	endfor

	if (!ParamIsDefault(PrintOutput))
		Print Waves2Convert
	endif

	SetDataFolder savedDF

End


//  DTS A very general purpose function for translating a string to a date.  Similar to function yyyymmddhhmmss2time() in General Macros but this is more flexibile
// Sample Usage: Format2Ssecs("Riverside_YYYYMMDD_hhmmss", Riverside_20001101_180456.dat") OR Format2Ssecs("Riverside_YYYYMMDD", Riverside_20001101_180456.dat")
////Adopted/renamed from DS_Gentools.ipf
Function SMPS_Format2Secs(Format, Str)		// MJC 7/16/2005 Calculates time (in Igor secs) of file, where format describes the file format
	//  	// Riverside_YYYYMMDD_hhmmss.dat (example)
	String format, str

	Variable year, month,day, hourv, minv, secv
	Variable yearpos, monthpos, daypos, hourpos, minpos, secpos

	NVar AIM_ExportVersion = root:Temporary:AIM_ExportVersion
	
	if( AIM_ExportVersion <= 9 )

		if (stringmatch(Format, "*YYYY*"))
			yearpos = strsearch(Format, "YYYY",0)
			year = str2num(str[yearpos, yearpos+3])
		elseif (stringmatch(Format, "*YY*"))
			yearpos = strsearch(Format, "YY",0)
			year = 2000 + str2num(str[yearpos, yearpos+1])
		else
			year = 1904
		endif

		if (stringmatch(Format, "*MM*"))
			monthpos= strsearch(Format, "MM",0)
			month = str2num(str[monthpos,monthpos+1])
		else
			month = 1
		endif

		if (stringmatch(Format, "*DD*"))
			daypos = strsearch(Format, "DD",0)
			day = str2num(str[daypos,daypos+1])
		else
			day = 1
		endif

		if (stringmatch(Format, "*hh*"))
			hourpos = strsearch(Format, "hh",0)
			hourv = str2num(str[hourpos, hourpos+1])
		else
			hourv = 0
		endif

		if (stringmatch(Format, "*mm*"))
			minpos = strsearch(Format, "mm",0)
			minv = str2num(str[minpos, minpos+1])
		else
			minv = 0
		endif

		if (stringmatch(Format, "*ss*"))
			secpos = strsearch(Format, "ss",0)
			secv = str2num(str[secpos, secpos+1])
		else
			secv = 0
		endif

		return date2secs(year, month, day) + (3600 * hourv) + (60 * minv) + secv

	Elseif( AIM_ExportVersion == 11 )
		if (stringmatch(Format, "*YYYY*"))
			yearpos = strsearch(Format, "YYYY",0)
			year = str2num(str[yearpos, yearpos+3])
		elseif (stringmatch(Format, "*YY*"))
			yearpos = strsearch(Format, "YY",0)
			year = 2000 + str2num(str[yearpos, yearpos+1])
		else
			year = 1904
		endif

		if (stringmatch(Format, "*MM*"))
			monthpos= strsearch(Format, "MM",0)
			month = str2num(str[monthpos,monthpos+1])
		else
			month = 1
		endif

		if (stringmatch(Format, "*DD*"))
			daypos = strsearch(Format, "DD",0)
			day = str2num(str[daypos,daypos+1])
		else
			day = 1
		endif

		return date2secs(year, month, day)

	Endif

End
