THIS CODE IS NOW MAINTAINED IN ANOTHER RESPOSITORY: https://gitlab.com/JimenezGroup/jg-utilities
(THIS CODE IS NO LONGER UPDATED HERE)


# SMPS_Loader_Plotter

JG_SMPS.ipf: A user friendly panel to ingest SMPS size distributions taken with the TSI AIM software into Igor experiments and time align them. Will  calculate and plot up number, surface and volume distributions (3D) and total number, surface, volume and condensational sinks for any time and size interval requested. 

Usage and versioning notes in ipf header.

Some description+graphics of historic versions can be found here: https://cires1.colorado.edu/jimenez-group/wiki/index.php/Analysis_Software#SMPS. 

A 5-min demo on the basics can be found here: https://www.youtube.com/watch?v=xRdaJNk8rUo


Reference as:
(Day et al. 2024)
Day, D.A., Cubison, M.J., Palm, B.B., Yun, S., J.L. Jimenez Research Group, Scanning Mobility Particle Sizer Loader and Plotter, University of Colorado, Boulder. https://gitlab.com/JimenezGroup/smps_loader_plotter Last accessed Day, Month, 2024
